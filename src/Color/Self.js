/*
| A color.
|
| Optionally including an alpha value.
*/
def.attributes =
{
	alpha: { type: [ 'undefined', 'number' ], json: true },
	red:   { type: 'integer', json: true },
	green: { type: 'integer', json: true },
	blue:  { type: 'integer', json: true }
};

def.json = true;

const Color = Self;

/*
| Color names
*/
def.staticLazy.black      = ( ) => Color.RGB(   0, 0,     0 );
def.staticLazy.blue       = ( ) => Color.RGB(   0, 0,   255 );
def.staticLazy.cyan       = ( ) => Color.RGB(   0, 255, 255 );
def.staticLazy.darkRed    = ( ) => Color.RGB( 128,   0,   0 );
def.staticLazy.green      = ( ) => Color.RGB(   0, 255,   0 );
def.staticLazy.gray       = ( ) => Color.RGB( 128, 128, 128 );
def.staticLazy.lightBlue  = ( ) => Color.RGB( 128, 128, 255 );
def.staticLazy.lightGray  = ( ) => Color.RGB( 244, 244, 244 );
def.staticLazy.lightGreen = ( ) => Color.RGB( 128, 255, 128 );
def.staticLazy.lightRed   = ( ) => Color.RGB( 255, 128, 128 );
def.staticLazy.orange     = ( ) => Color.RGB( 255, 165,   0 );
def.staticLazy.red        = ( ) => Color.RGB( 255,   0,   0 );
def.staticLazy.white      = ( ) => Color.RGB( 255, 255, 255 );

/*
| Shortcut creator.
*/
def.static.RGB =
	function( red, green, blue )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	return Color.create( 'red', red, 'green', green, 'blue', blue );
};

/*
| Shortcut creator.
*/
def.static.RGBA =
	function( red, green, blue, alpha )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	return Color.create( 'red', red, 'green', green, 'blue', blue, 'alpha', alpha );
};

/*
| Color text understood by browser.
*/
def.lazy.css =
	function( )
{
	return(
		this.alpha
		? 'rgba( ' + this.red + ', ' + this.green + ', ' + this.blue + ', ' + this.alpha + ' )'
		: 'rgb( ' + this.red + ', ' + this.green + ', ' + this.blue + ' )'
	);
};
