/*
| A font family.
*/
def.attributes =
{
	// name of the family
	name: { type: 'string', json: true },

	// opentype font implementation
	_opentype: { type: 'protean' },
};

def.json = true;

import { Self as FontBland } from '{Font/Bland}';

/*
| Returns a FontBland object for this fontFamily.
*/
def.lazyFunc.FontBland =
	function( size )
{
	return FontBland.create( 'family', this, 'size', size );
};

/*
| The cap height for a font.
*/
def.lazy.capHeight =
	function( )
{
	const opentype = this._opentype;
	const chars = 'HIKLEFJMNTZBDPRAGOQSUVWXY';
	const glyphs = opentype.glyphs;

	for( let c of chars )
	{
		const idx = opentype.charToGlyphIndex( c );
		if( idx <= 0 )
		{
			continue;
		}
		return glyphs.get( idx ).getMetrics( ).yMax;
	}

	// should not happen
	throw new Error( );
};
