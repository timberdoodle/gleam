/*
| Convex hull algorithm using graham filter.
*/
def.abstract = true;

import { Self as Line } from '{Figure/Path/Line}';

/*
| Compares points radial; for convex hull creation.
*/
const comparePRadial =
	function( pa, pb )
{
	return( ( pa.x - this.x )*( pb.y - this.y ) - ( pb.x - this.x )*( pa.y - this.y ) );
};

/*
| Filters out points not part of the hull.
|
| ~pointArray: radially sorted point Array
*/
def.static._filter =
	function( pointArray )
{
	const stack = [ ];
	for( let p of pointArray )
	{
		for(;;)
		{
			const slen = stack.length;
			if( slen < 2 ) break;
			const pt2 = stack[ slen - 2 ];
			const pt1 = stack[ slen - 1 ];
			const line = Line.P0P1( pt2, pt1 );
			if( line.sideOfPoint( p ) > 0 ) stack.pop( );
			else break;
		}
		stack.push( p );
	}
	return stack;
};

/*
| Returns an array of points of the convex hull
|
| p0 is already guaranteed to be part of the hull.
*/
def.static.fromPointsP0h =
	function( /* p0, p1, ... pn */ )
{
	const ps = Array.prototype.slice.call( arguments );
	ps.sort( comparePRadial.bind( ps[ 0 ] ) );
	return Self._filter( ps );
};
