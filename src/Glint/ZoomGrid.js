/*
| A zooming grid in a display.
*/
def.attributes =
{
	// color of the grid (at max distinction)
	colorHeavy: { type: 'Color/Self', json: true },

	// color of the background (at min distinction)
	colorLight: { type: 'Color/Self', json: true },

	// the grid zooming factor
	grid: { type: 'number', json: true },

	// name of the clint for debugging & testsuite
	name: { type: [ 'undefined', 'string' ], json: true },

	// the offset of the (major) grid
	offset: { type: 'Figure/Point', json: true },

	// the total size of the grid
	size: { type: 'Figure/Size', json: true },

	// the distance of the (major) grid point
	spacing: { type: 'Figure/Point', json: true },
};

def.json = true;
