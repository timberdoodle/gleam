/*
| An arrow is a line with arrow heads.
*/
def.attributes =
{
	// arrow head size
	arrowHeadSize: { type: 'number' },

	// "none" or "arrow"
	end0: { type: 'string' },

	// "none" or "arrow"
	end1: { type: 'string' },

	// connect to this figure
	joint0: { type: [ '< Figure/Shape/Types', 'Figure/Path/Self', 'Figure/Point' ] },

	// connect to this figure
	joint1: { type: [ '< Figure/Shape/Types', 'Figure/Path/Self', 'Figure/Point' ] },

	// width of the base line
	widthBase: { type: 'number' },
};

def.json = true;

import { Self as Path          } from '{Figure/Path/Self}';
import { Self as Segment       } from '{Figure/Path/Segment}';
import { Self as TransformBase } from '{Transform/Base}';

const atan2 = Math.atan2;
const cos = Math.cos;
const pi = Math.PI;
const pi1d12 = pi / 12;
const sin = Math.sin;
const sq3 = Math.sqrt( 3 );

/*
| Expands( or shrinks ) the shape by d.
*/
def.lazyFunc.envelope =
	function( d )
{
/**/if( CHECK && typeof( d ) !== 'number' ) throw new Error( );

	return this.path.envelope( d );
};

/*
| Returns the arrow path going from joint0 to joint1.
| A joint is either a point, path or shape.
*/
def.lazy.path =
	function( )
{
	const joint0 = this.joint0;
	const joint1 = this.joint1;
	const end0 = this.end0;
	const end1 = this.end1;
	const arrowHeadSize = this.arrowHeadSize;
	const widthBase = this.widthBase;

	const segment = Segment.Connection( joint0, joint1 );
	const p0 = segment.p0;
	const p1 = segment.p1;
	const plan = [ 'pc', segment.pc ];

	switch( end0 )
	{
		case 'arrow':
		{
			// degree of arrow tail
			const d = atan2( p0.y - p1.y, p0.x - p1.x );
			// degree of arrow head
			const ad = pi1d12;
			// arrow span, the arrow is formed as a hexagon piece
			const ms = 2 / sq3 * arrowHeadSize;

			const arrowBase = p0.add( -ms * cos( d ), -ms * sin( d ) );

			plan.push(
				'start', arrowBase,
				'line',
					p0.add(
						-arrowHeadSize * cos( d + ad ),
						-arrowHeadSize * sin( d + ad )
					),
				'line', p0,
				'line',
					p0.add(
						-arrowHeadSize * cos( d - ad ),
						-arrowHeadSize * sin( d - ad )
					),
				'line', arrowBase
			);
			break;
		}

		case 'none':
			if( widthBase === 0 )
			{
				plan.push( 'start', p0, 'blind' );
			}
			else
			{
				// FIXME this is wrong, make actual width and no blind
				plan.push( 'start', p0, 'blind' );
			}
			break;

		// unknown arrow end
		default : throw new Error( );
	}

	switch( end1 )
	{
		case 'none':
		{
			plan.push( 'line', p1, 'blind' );
			break;
		}

		case 'arrow':
		{
			// degree of arrow tail
			const d = atan2( p1.y - p0.y, p1.x - p0.x );
			// degree of arrow head
			const ad = pi1d12;
			// arrow span, the arrow is formed as a hexagon piece
			const ms = 2 / sq3 * arrowHeadSize;

			const arrowBase = p1.add( -ms * cos( d ), -ms * sin( d ) );

			plan.push(
				'line', arrowBase,
				'line',
					p1.add(
						-arrowHeadSize * cos( d + ad ),
						-arrowHeadSize * sin( d + ad )
					),
				'line', p1,
				'line',
					p1.add(
						-arrowHeadSize * cos( d - ad ),
						-arrowHeadSize * sin( d - ad )
					),
				'line', arrowBase
			);
			break;
		}

		// unknown arrow end
		default: throw new Error( );
	}
	plan.push( 'line', 'close' );
	return Path.Plan.apply( undefined, plan );
};

/*
| Returns this transformed shape.
*/
def.proto.transform =
	function( transform )
{
/**/if( CHECK && !TransformBase.isa( transform ) ) throw new Error( );

	return(
		this.create(
			'arrowHeadSize', transform.d( this.arrowHeadSize ),
			'joint0',        this.joint0.transform( transform ),
			'joint1',        this.joint1.transform( transform ),
			'widthBase',     transform.d( this.widthBase ),
		)
	);
};
