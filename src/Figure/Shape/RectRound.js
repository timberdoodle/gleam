/*
| A rectangle with rounded corners.
|
| RectRounds are immutable objects.
|
|      <--> a
|      |  |
|  pos + .----------------. - - - A - - - A
|      .'                  `. _ _ V b     | height
|      |                    |             |
|      |                    |             |
|      |                    |             |
|      |                    |             |
|      '.                  .'             |
|        `----------------' + - - - - - - V
|      |                    |
|      <--------------------> width
*/
def.attributes =
{
	// horizonal rounding
	a: { type: 'number', json: true },

	// vertical rounding
	b: { type: 'number', json: true },

	// height
	height: { type: 'number', json: true },

	// position
	pos: { type: 'Figure/Point', json: true },

	// width
	width: { type: 'number', json: true },
};

def.json = true;

import { Self as Display       } from '{Display/Canvas/Self}';
import { Self as Path          } from '{Figure/Path/Self}';
import { Self as TransformBase } from '{Transform/Base}';

/*
| Returns the shape moved by point or x/y.
*/
def.proto.add =
	function( px, y )
{
	return this.create( 'pos', this.pos.add( px, y ) );
};

/*
| Expands( or shrinks ) the round rect by d.
*/
def.lazyFunc.envelope =
	function( d )
{
/**/if( CHECK && typeof( d ) !== 'number' ) throw new Error( );

	return(
		this.create(
			'a',      this.a + d,
			'b',      this.b + d,
			'pos',    this.pos.add( -d, -d ),
			'height', this.height + 2 * d,
			'width',  this.width  + 2 * d
		)
	);
};

/*
| Gets the intersections with something else.
|
| ~figure: figure to intersect with
*/
def.proto.intersectsPoint =
	function( figure )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	return this.path.intersectsPoint( figure );
};

/*
| The path of the shape.
*/
def.lazy.path =
	function( )
{
	const a = this.a;
	const b = this.b;
	const h = this.height;
	const p = this.pos;
	const w = this.width;

	if( b * 2 >= h )
	{
		// the sides are fully rounded
		return(
			Path.Plan(
				'pc', this.pc,
				'start', p.add( 0 , b ),
				'qbend', p.add( a , 0 ),
				'line',  p.add( w - a , 0 ),
				'qbend', p.add( w , b ),
				'qbend', p.add( w - a , h ),
				'line',  p.add( a , h ),
				'qbend', 'close'
			)
		);
	}
	else
	{
		return(
			Path.Plan(
				'pc', this.pc,
				'start', p.add( 0 , b ),
				'qbend', p.add( a , 0 ),
				'line',  p.add( w - a , 0 ),
				'qbend', p.add( w , b ),
				'line',  p.add( w , h - b ),
				'qbend', p.add( w - a , h ),
				'line',  p.add( a , h ),
				'qbend', p.add( 0 , h - b ),
				'line', 'close'
			)
		);
	}
};

/*
| Point in the center.
*/
def.lazy.pc =
	function( )
{
	return this.pos.add( this.width / 2, this.height / 2 );
};

/*
| East point.
*/
def.lazy.pe =
	function( )
{
	return this.pos.add( this.width, this.height / 2 );
};

/*
| North point.
*/
def.lazy.pn =
	function( )
{
	return this.pos.add( this.width / 2, 0 );
};

/*
| North east point.
*/
def.lazy.pne =
	function( )
{
	return this.pos.add( this.width, 0 );
};

/*
| South point.
*/
def.lazy.ps =
	function( )
{
	return this.pos.add( this.width / 2, this.height );
};

/*
| South east point.
*/
def.lazy.pse =
	function( )
{
	return this.pos.add( this.width, this.height );
};

/*
| South west point.
*/
def.lazy.psw =
	function( )
{
	return this.pos.add( 0, this.height );
};

/*
| West point.
*/
def.lazy.pw =
	function( )
{
	return this.pos.add( 0, this.height / 2 );
};

/*
| Returns a transformed shape.
*/
def.proto.transform =
	function( transform )
{
/**/if( CHECK && !TransformBase.isa( transform ) ) throw new Error( );

	return(
		this.create(
			'pos', this.pos.transform( transform ),
			'width', transform.d( this.width ),
			'height', transform.d( this.height ),
			'a', transform.d( this.a ),
			'b', transform.d( this.b )
		)
	);
};

/*
| Returns true if p is within the shape.
*/
def.proto.within =
	function( p )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	return Display.helper.within( p, this );
};
