/*
| A string (text) glint for gleam.
*/
def.attributes =
{
	// horizonal alignment
	align: { type: 'string', defaultValue: '"left"', json: true },

	// vertical alignment
	// 'alphabetic' or 'middle'
	base: { type: 'string', defaultValue: '"alphabetic"', json: true },

	// the font color to display
	fontColor: { type: 'Font/Color', json: true },

	// name of the glint for debugging & testsuite
	name: { type: [ 'undefined', 'string' ], json: true },

	// where to draw it
	p: { type: 'Figure/Point', json: true },

	// the canvas resolution
	resolution: { type: 'Display/Canvas/Resolution', json: true },

	// if defined, rotation in radians
	rotation: { type: [ 'undefined', '< Figure/Angle/Types' ], json: true },

	// the string to display
	string: { type: 'string', json: true },
};

def.json = true;

import { Self as AngleE } from '{Figure/Angle/E}';

/*
| The font string of the text glint.
*/
def.lazy.fontString =
	function( )
{
	const rotation = this.rotation;

	if( !rotation || rotation === AngleE )
	{
		return this.fontColor.StringNormal( this.string );
	}
	else
	{
		return this.fontColor.StringRotate( this.string, rotation );
	}
};
