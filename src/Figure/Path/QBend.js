/*
| A quarter bend ( quarter of an ellipsis ).
*/
def.attributes =
{
	// start point
	p0: { type: 'Figure/Point', json: true },

	// end point
	p1: { type: 'Figure/Point', json: true },

	// if true it bends counter-clockwise
	ccw: { type: 'boolean', json: true },
};

def.json = true;

import { Self as Point   } from '{Figure/Point}';
import { Self as Ray     } from '{Figure/Path/Ray}';
import { Self as Rect    } from '{Figure/Shape/Rect}';
import { Self as Segment } from '{Figure/Path/Segment}';

//const xor = ( a, b ) => ( ( a && b ) || ( !a && !b ) );

/*
| Moves the qbend by p or x/y.
*/
def.proto.add =
	function( /* p or x,y */ )
{
	const p0 = this.p0;
	const p1 = this.p1;
	return(
		this.create(
			'p0', p0.add.apply( p0, arguments ),
			'p1', p1.add.apply( p1, arguments )
		)
	);
};

/*
| Returns a qbend with distance d next to this line segment.
*/
def.lazyFunc.envelope =
	function( d )
{
	const p0 = this.p0;
	const p1 = this.p1;
	let x0 = p0.x;
	let y0 = p0.y;
	let x1 = p1.x;
	let y1 = p1.y;
	if( x0 < x1 )
	{
		if( y0 < y1 ) { y0 -= d; x1 += d; }
		else { x0 -= d; y1 -= d; }
	}
	else
	{
		if( y0 < y1 ) { x0 += d; y1 += d; }
		else { y0 += d; x1 -= d; }
	}

	return this.create( 'p0', Point.XY( x0, y0 ), 'p1', Point.XY( x1, y1 ) );
};

/*
| Get the first intersection of the qbend with another figure.
*/
def.proto.intersectsPoint =
	function( figure )
{
	if( figure.ti2ctype === Segment ) return this._intersectsSegmentPoint( figure );
	if( figure.ti2ctype === Ray ) return this._intersectsRayPoint( figure );
	throw new Error( );
};

/*
| Moves the qbend by p or x/y.
*/
def.proto.sub =
	function( /* p or x,y */ )
{
	const p0 = this.p0;
	const p1 = this.p1;
	return(
		this.create(
			'p0', p0.sub.apply( p0, arguments ),
			'p1', p1.sub.apply( p1, arguments )
		)
	);
};

/*
| Returns a transformed qbend.
*/
def.proto.transform =
	function( transform )
{
	return(
		this.create(
			'p0', this.p0.transform( transform ),
			'p1', this.p1.transform( transform )
		)
	);
};

/*
| The zone of the qbend.
*/
def.lazy.zone =
	function( )
{
	return Rect.Arbitrary( this.p0, this.p1 );
};


/*
| Gets the first intersection of the qbend with a line segment.
|
| ~segment: the segment to intersect with.
*/
def.proto._intersectsRayPoint =
	function( ray )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( ray.ti2ctype !== Ray ) throw new Error( );
/**/}

	const p0 = this.p0;
	const p1 = this.p1;
	const p0x = p0.x;
	const p0y = p0.y;
	const p1x = p1.x;
	const p1y = p1.y;
	const dx = p1x - p0x;
	const dy = p1y - p0y;
	const dxy = dx * dy;
	let a, b;
	let cx, cy;
	let ps0, ps1;

	ps0 = ray.p0;
	ps1 = ray.p1;

	if( dxy > 0 )
	{
		// center of the qbend
		cx = p0.x;
		cy = p1.y;
		a  = Math.abs( p1x - cx );
		b  = Math.abs( p0y - cy );
	}
	else
	{
		// center of the qbend
		cx = p1x;
		cy = p0y;
		a = Math.abs( p0x - cx );
		b = Math.abs( p1y - cy );
	}

	const ps0x = ps0.x;
	const ps0y = ps0.y;
	const ps1x = ps1.x;
	const ps1y = ps1.y;

	if( ray.angle.steep )
	{
		const k = ( ps0x - ps1x ) / ( ps0y - ps1y );
		const d = ( ps1x - cx ) - k * ( ps1y - cy );

		// x^2 / a^2 + y^2 / b^2 = 1
		// x = k * y + d

		// (k * y + d )^2 / a^2 + y^2 / b^2 = 1
		// k^2 * y^2 / a^2 + 2 * k * y * d / a^2 + d^2 / a^2 + y^2 / b^2 = 1
		// y^2 * ( 1 / b^2 + k^2 / a^2 ) + y ( 2 * k * d / a^2 ) + d^2 / a^2 - 1 = 0

		const qa = 1 / (b * b) + k * k / ( a * a );
		const qb = 2 * k * d / ( a * a );
		const qc = d * d / ( a * a ) - 1;

		let y0 = ( -qb + Math.sqrt ( qb * qb - 4 * qa * qc ) ) / ( 2 * qa );
		let y1 = ( -qb - Math.sqrt ( qb * qb - 4 * qa * qc ) ) / ( 2 * qa );

		let x0 = k * y0 + d;
		let x1 = k * y1 + d;

		x0 += cx;
		y0 += cy;
		x1 += cx;
		y1 += cy;

		if(
			// valid result?
			!Number.isNaN( y0 )
			// on the qbend quadrant?
			&& ( x0 >= p0x && x0 <= p1x || x0 <= p0x && x0 >= p1x )
			&& ( y0 >= p0y && y0 <= p1y || y0 <= p0y && y0 >= p1y )
			// on the ray?
			&& ( ps1y >= ps0y ? y0 >= ps0y : y0 < ps0y )
		) return Point.XY( x0, y0 );

		if(
			// valid result?
			!Number.isNaN( y1 )
			// on the qbend quadrant?
			&& ( x1 >= p0x && x1 <= p1x || x1 <= p0x && x1 >= p1x )
			&& ( y1 >= p0y && y1 <= p1y || y1 <= p0y && y1 >= p1y )
			// on the ray?
			&& ( ps1y >= ps0y ? y1 >= ps0y : y1 < ps0y )
		) return Point.XY( x1, y1 );
	}
	else
	{
		const k = ( ps0y - ps1y ) / ( ps0x - ps1x );
		const d = ( ps1y - cy ) - k * ( ps1x - cx );

		// x^2 / a^2 + y^2 / b^2 = 1
		// y = k * x + d
		// x^2 / a^2 + ( k * x + d )^2 / b^2 = 1
		// x^2 / a^2 + k^2 * x^2 / b^2 + 2 * k * x * d / b^2 + d^2 / b^2 = 1
		// x^2 ( 1 / a^2 + k^2 / b^2 ) + x ( 2 * k * d / b^2 ) + d^2 / b^2 - 1 = 0

		const qa = 1 / (a * a) + k * k / ( b * b );
		const qb = 2 * k * d / ( b * b );
		const qc = d * d / ( b * b ) - 1;

		//  ps0x > cx
		// x = Math.sqrt(  1 / ( 1 / ( a * a ) + k * k / ( b * b ) ) );
		let x0 = ( -qb + Math.sqrt ( qb * qb - 4 * qa * qc ) ) / ( 2 * qa );
		let x1 = ( -qb - Math.sqrt ( qb * qb - 4 * qa * qc ) ) / ( 2 * qa );

		let y0 = k * x0 + d;
		let y1 = k * x1 + d;

		x0 += cx;
		y0 += cy;
		x1 += cx;
		y1 += cy;

		if(
			// valid result?
			!Number.isNaN( x0 )
			// on the qbend quadrant?
			&& ( x0 >= p0x && x0 <= p1x || x0 <= p0x && x0 >= p1x )
			&& ( y0 >= p0y && y0 <= p1y || y0 <= p0y && y0 >= p1y )
			// on the ray?
			&& ( ps1x >= ps0x ? x0 >= ps0x : x0 < ps0x )
		) return Point.XY( x0, y0 );

		if(
			// valid result?
			!Number.isNaN( x1 )
			// on the qbend quadrant?
			&& ( x1 >= p0x && x1 <= p1x || x1 <= p0x && x1 >= p1x )
			&& ( y1 >= p0y && y1 <= p1y || y1 <= p0y && y1 >= p1y )
			// on the ray?
			&& ( ps1x >= ps0x ? x1 >= ps0x : x1 < ps0x )
		) return Point.XY( x1, y1 );
	}

	// no valid intersection
	return undefined;
};

/*
| Gets the first intersection of the qbend with a line segment.
|
| ~segment: the segment to intersect with.
*/
def.proto._intersectsSegmentPoint =
	function( segment )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( segment.ti2ctype !== Segment ) throw new Error( );
/**/}

	const p0 = this.p0;
	const p1 = this.p1;
	const p0x = p0.x;
	const p0y = p0.y;
	const p1x = p1.x;
	const p1y = p1.y;
	const dx = p1x - p0x;
	const dy = p1y - p0y;
	const dxy = dx * dy;
	let a, b;
	let cx, cy;
	let ps0, ps1;

	ps0 = segment.p0;
	ps1 = segment.p1;

	if( dxy > 0 )
	{
		// center of the qbend
		cx = p0.x;
		cy = p1.y;
		a  = Math.abs( p1x - cx );
		b  = Math.abs( p0y - cy );
	}
	else
	{
		// center of the qbend
		cx = p1x;
		cy = p0y;
		a = Math.abs( p0x - cx );
		b = Math.abs( p1y - cy );
	}

	const ps0x = ps0.x;
	const ps0y = ps0.y;
	const ps1x = ps1.x;
	const ps1y = ps1.y;

	//if(
	//	( ps0x < cx || dy <= 0 ) && ( ps0x > cx || dy >= 0 )
	//	|| ( ps0y < cy || dx >= 0 ) && ( ps0y > cy || dx <= 0 )
	//) return;

	if( segment.angle.steep )
	{
		const k = ( ps0x - ps1x ) / ( ps0y - ps1y );
		const d = ( ps1x - cx ) - k * ( ps1y - cy );

		// x^2 / a^2 + y^2 / b^2 = 1
		// x = k * y + d

		// (k * y + d )^2 / a^2 + y^2 / b^2 = 1
		// k^2 * y^2 / a^2 + 2 * k * y * d / a^2 + d^2 / a^2 + y^2 / b^2 = 1
		// y^2 * ( 1 / b^2 + k^2 / a^2 ) + y ( 2 * k * d / a^2 ) + d^2 / a^2 - 1 = 0

		const qa = 1 / (b * b) + k * k / ( a * a );
		const qb = 2 * k * d / ( a * a );
		const qc = d * d / ( a * a ) - 1;

		let y0 = ( -qb + Math.sqrt ( qb * qb - 4 * qa * qc ) ) / ( 2 * qa );
		let y1 = ( -qb - Math.sqrt ( qb * qb - 4 * qa * qc ) ) / ( 2 * qa );

		let x0 = k * y0 + d;
		let x1 = k * y1 + d;

		x0 += cx;
		y0 += cy;
		x1 += cx;
		y1 += cy;

		if(
			// valid result?
			!Number.isNaN( y0 )
			// on the qbend quadrant?
			&& ( x0 >= p0x && x0 <= p1x || x0 <= p0x && x0 >= p1x )
			&& ( y0 >= p0y && y0 <= p1y || y0 <= p0y && y0 >= p1y )
			// on the segment?
			&& ( y0 >= ps0y && y0 <= ps1y || y0 <= ps0y && y0 >= ps1y )
		) return Point.XY( x0, y0 );

		if(
			// valid result?
			!Number.isNaN( y1 )
			// on the qbend quadrant?
			&& ( x1 >= p0x && x1 <= p1x || x1 <= p0x && x1 >= p1x )
			&& ( y1 >= p0y && y1 <= p1y || y1 <= p0y && y1 >= p1y )
			// on the segment?
			&& ( y1 >= ps0y && y1 <= ps1y || y1 <= ps0y && y1 >= ps1y )
		) return Point.XY( x1, y1 );
	}
	else
	{
		const k = ( ps0y - ps1y ) / ( ps0x - ps1x );
		const d = ( ps1y - cy ) - k * ( ps1x - cx );

		// x^2 / a^2 + y^2 / b^2 = 1
		// y = k * x + d
		// x^2 / a^2 + ( k * x + d )^2 / b^2 = 1
		// x^2 / a^2 + k^2 * x^2 / b^2 + 2 * k * x * d / b^2 + d^2 / b^2 = 1
		// x^2 ( 1 / a^2 + k^2 / b^2 ) + x ( 2 * k * d / b^2 ) + d^2 / b^2 - 1 = 0

		const qa = 1 / (a * a) + k * k / ( b * b );
		const qb = 2 * k * d / ( b * b );
		const qc = d * d / ( b * b ) - 1;

		//  ps0x > cx
		// x = Math.sqrt(  1 / ( 1 / ( a * a ) + k * k / ( b * b ) ) );
		let x0 = ( -qb + Math.sqrt ( qb * qb - 4 * qa * qc ) ) / ( 2 * qa );
		let x1 = ( -qb - Math.sqrt ( qb * qb - 4 * qa * qc ) ) / ( 2 * qa );

		let y0 = k * x0 + d;
		let y1 = k * x1 + d;

		x0 += cx;
		y0 += cy;
		x1 += cx;
		y1 += cy;

		if(
			// valid result?
			!Number.isNaN( x0 )
			// on the qbend quadrant?
			&& ( x0 >= p0x && x0 <= p1x || x0 <= p0x && x0 >= p1x )
			&& ( y0 >= p0y && y0 <= p1y || y0 <= p0y && y0 >= p1y )
			// on the segment?
			&& ( x0 >= ps0x && x0 <= ps1x || x0 <= ps0x && x0 >= ps1x )
		) return Point.XY( x0, y0 );
		if(
			// valid result?
			!Number.isNaN( x1 )
			// on the qbend quadrant?
			&& ( x1 >= p0x && x1 <= p1x || x1 <= p0x && x1 >= p1x )
			&& ( y1 >= p0y && y1 <= p1y || y1 <= p0y && y1 >= p1y )
			// on the segment?
			&& ( x1 >= ps0x && x1 <= ps1x || x1 <= ps0x && x1 >= ps1x )
		) return Point.XY( x1, y1 );
	}

	// no valid intersection
	return undefined;
};
