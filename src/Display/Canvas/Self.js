/*
| Displays stuff using a HTML5 canvas renderer.
*/
def.attributes =
{
	// if set the canvas is opaque and has a background
	background: { type: [ 'undefined', 'Color/Self' ] },

	// the glint list to display
	glint: { type: [ 'undefined', '< Glint/Types' ] },

	// the canvas resolution
	resolution: { type: 'Display/Canvas/Resolution' },

	// the size of the display as logical drawing pixels
	// is size.scale( resolution.ratio )
	size: { type: 'Figure/Size' },

	// the size of the display on screen
	screenSize: { type: 'Figure/Size' },

	// the html canvas
	_canvas: { type: 'protean' },
};

//const glintCacheLimit = 32767;
const glintCacheLimit = 32767 * 16;
//const glintCacheLimit = Number.POSITIVE_INFINITY;

import { Self as Arrow           } from '{Figure/Shape/Arrow}';
import { Self as Bezier          } from '{Figure/Path/Bezier}';
import { Self as Blind           } from '{Figure/Path/Blind}';
import { Self as Color           } from '{Color/Self}';
import { Self as Dashed          } from '{Style/Dashed}';
import { Self as Ellipse         } from '{Figure/Shape/Ellipse}';
import { Self as FigureList      } from '{Figure/List}';
import { Self as GlintFigure     } from '{Glint/Figure}';
import { Self as GlintImageData  } from '{Glint/ImageData}';
import { Self as GlintMask       } from '{Glint/Mask}';
import { Self as GlintString     } from '{Glint/String}';
import { Self as GlintTransform  } from '{Glint/Transform}';
import { Self as GlintWindow     } from '{Glint/Window}';
import { Self as GlintZoomGrid   } from '{Glint/ZoomGrid}';
import { Self as GradientAskew   } from '{Style/Gradient/Askew}';
import { Self as GradientRadial  } from '{Style/Gradient/Radial}';
import { Self as Line            } from '{Figure/Path/Line}';
import { Self as ListGlint       } from '{list@<Glint/Types}';
import { Self as Path            } from '{Figure/Path/Self}';
import { Self as Pin             } from '{Figure/Shape/Pin}';
import { Self as Point           } from '{Figure/Point}';
import { Self as Ray             } from '{Figure/Path/Ray}';
import { Self as Rect            } from '{Figure/Shape/Rect}';
import { Self as RectRound       } from '{Figure/Shape/RectRound}';
import { Self as RectRoundExt    } from '{Figure/Shape/RectRoundExt}';
import { Self as Resolution      } from '{Display/Canvas/Resolution}';
import { Self as RingSector      } from '{Figure/Shape/RingSector}';
import { Self as QBend           } from '{Figure/Path/QBend}';
import { Self as QCurve          } from '{Figure/Path/QCurve}';
import { Self as Segment         } from '{Figure/Path/Segment}';
import { Self as Size            } from '{Figure/Size}';
import { Self as TransformBase   } from '{Transform/Base}';
import { Self as TransformNormal } from '{Transform/Normal}';

const round = Math.round;

let createCanvas;

if( !NODE ) createCanvas = ( ) => document.createElement( 'canvas' );

/*
| Creates a display around an existing HTML canvas.
|
| ~canvas: the canvas to create around
| ~screenSize: size of the canvas
| ~resolution: the canvas resolution
| ~glint: the content
| ~background: the background
*/
def.static.AroundHTMLCanvas =
	function( canvas, screenSize, resolution, glint, background )
{
/**/if( CHECK && arguments.length !== 5 ) throw new Error( );

	const size = screenSize.scaleTrunc( resolution.ratio );
	screenSize = size.scale( 1 / resolution.ratio );

	canvas.width = size.width;
	canvas.height = size.height;

	if( !NODE )
	{
		const style = canvas.style;
		style.width = screenSize.width + 'px';
		style.height = screenSize.height + 'px';
	}

	return(
		Self.create(
			'background', background,
			'glint', glint,
			'resolution', resolution,
			'screenSize', screenSize,
			'size', size,
			'_canvas', canvas
		)
	);
};

/*
| Creates a new blank transparent canvas.
|
| ~screenSize: screenSize of the canvas
| ~resolution: canvas resolution
| ~glint:      glint to draw
| ~background: background
*/
def.static.NewCanvas =
	function( screenSize, resolution, glint, background )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length < 3 || arguments.length > 4 ) throw new Error( );
/**/	if( screenSize.ti2ctype !== Size ) throw new Error( );
/**/	if( resolution.ti2ctype !== Resolution ) throw new Error( );
/**/}

	const size = screenSize.scaleTrunc( resolution.ratio );
	screenSize = size.scale( 1 / resolution.ratio );

	const canvas = createCanvas( );
	canvas.width = size.width;
	canvas.height = size.height;

	return(
		Self.create(
			'background', background,
			'glint', glint,
			'resolution', resolution,
			'screenSize', screenSize,
			'size', size,
			'_canvas', canvas
		)
	);
};

/*
| Resizes the canvas.
|
| ~screenSize: the new canvas screen size
| ~resolution: new resolution
*/
def.proto.resize =
	function( screenSize, resolution )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( screenSize.ti2ctype !== Size ) throw new Error( );
/**/	if( resolution.ti2ctype !== Resolution ) throw new Error( );
/**/}

	const size = screenSize.scaleTrunc( resolution.ratio );
	screenSize = size.scale( 1 / resolution.ratio );

	const cv = this._canvas;
	cv.height = size.height;
	cv.width = size.width;

	const style = cv.style;
	style.height = screenSize.height + 'px';
	style.width = screenSize.width + 'px';

	return(
		this.create(
			'resolution', resolution,
			'screenSize', screenSize,
			'size', size
		)
	);
};

/*
| A hidden helper canvas used by within( )
*/
def.staticLazy.helper = ( ) =>
	Self.NewCanvas( Size.WH( 10, 10 ), Resolution.Identity, pass );

/*
| Renders the display.
*/
def.proto.render =
	function( )
{
	const size = this.size;

	if( this.background )
	{
		this._cx.fillStyle = this.background.css;
		this._cx.fillRect( 0, 0, size.width, size.height );
	}
	else
	{
		this._cx.clearRect( 0, 0, size.width, size.height );
	}

	this._renderGlint( this.glint, TransformNormal.singleton );

	// dummy so the render is defined
	// caching?
	//return true;
};

/*
| Returns true if p is within the shape.
*/
def.proto.within =
	function( p, shape )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( p.ti2ctype !== Point ) throw new Error( );
/**/}

	const transform = TransformNormal.singleton;
	if( shape.ti2ctype === FigureList )
	{
		for( let s of shape )
		{
			if( this.within( p, s ) ) return true;
		}
		return false;
	}

	switch( shape.ti2ctype )
	{
		case Ellipse:
		case Pin:
		case RectRound:
		case RectRoundExt:
		case RingSector:
		{
			this._cx.beginPath( );
			this._sketchPath( shape.path, transform, 0, false );
			break;
		}

		case Rect:
		{
			const x = p.x;
			const y = p.y;
			const ps = shape.p;
			return(
				x >= ps.x
				&& y >= ps.y
				&& x <= ps.x + shape.width
				&& y <= ps.y + shape.height
			);
		}

		case Path:
		{
			this._cx.beginPath( );
			this._sketchPath( shape, transform, 0, false );
			break;
		}

		default: throw new Error( );
	}

	return this._cx.isPointInPath( this._x( p.x, transform ), this._y( p.y, transform ) );
};

/*
| The canvas context
| Turns on performance vs. quality settings.
*/
def.lazy._cx =
	function( )
{
	const cv = this._canvas;

	const cx =
		this.background
		? cv.getContext( '2d', { alpha: false } )
		: cv.getContext( '2d', { } );

	cx.imageSmoothingEnabled = false;
	cx.lineCap = 'square';
	return cx;
};

/*
| Moves a value onto pixel grid.
| Rounds the value and respects the device pixel ratio.
|
| ~v: the value to round
| ~noround: do not do rounding
*/
def.proto._d =
	function( v, noround )
{
	return v;
};

/*
| Draws a fill.
|
| ~fill: the fill color/gradient
| ~figure: a figure to sketch
| ~transform: transforms everything by this
*/
def.proto._fill  =
	function( fill, figure, transform, nogrid )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	const cx = this._cx;
	this._sketch( figure, transform, 0, nogrid );
	this._setFillStyle( fill, figure, transform );
	cx.fill( );
};


/*
| Renders a glint.
|
| ~glint: the glint list to render
| ~transform: transforms all rendering by this
*/
def.proto._renderGlint =
	function( glint, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	this[ Self._renderGlintMap.get( glint.ti2ctype ) ]( glint, transform );
};

/*
| Renders a glint twig.
|
| ~glint: the glint list to render
| ~transform: offset all rendering by this
*/
def.proto._renderListGlint =
	function( glint, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( glint.ti2ctype !== ListGlint ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	for( let g of glint )
	{
		this._renderGlint( g, transform );
	}
};

/*
| Maps the various glints to their rendering function
*/
def.staticLazy._renderGlintMap =
	function( )
{
	const m = new Map( [
		[ GlintFigure,    '_renderFigure'    ],
		[ GlintImageData, '_renderImageData' ],
		[ GlintMask,      '_renderMask'      ],
		[ GlintString,    '_renderString'    ],
		[ GlintTransform, '_renderTransform' ],
		[ GlintWindow,    '_renderWindow'    ],
		[ GlintZoomGrid,  '_renderZoomGrid'  ],
		[ ListGlint,      '_renderListGlint' ],
	] );

	return m;
};

/*
| Renders masked stuff.
|
| ~glint: the glint list to render
| ~transform: transforms all rendering by this
*/
def.proto._renderMask =
	function( glint, transform )
{
/**/if( CHECK )
/**/{
/**/	if( glint.ti2ctype !== GlintMask ) throw new Error();
/**/	if( !TransformBase.isa( transform ) ) throw new Error();
/**/}

	const cx = this._cx;
	const size = this.size;
	const w = this._x( size.width, transform );
	const h = this._y( size.height, transform );
	cx.save( );
	const outline = glint.outline;

	if( outline.ti2ctype === FigureList )
	{
		for( let oa of outline )
		{
			cx.beginPath( );
			if( glint.reversed )
			{
				cx.moveTo( 0, 0 );
				cx.lineTo( 0, h );
				cx.lineTo( w, h );
				cx.lineTo( w, 0 );
				cx.lineTo( 0, 0 );
			}
			this._sketch( oa, transform, 0, false );
			cx.clip( );
		}
	}
	else
	{
		cx.beginPath( );
		if( glint.reversed )
		{
			cx.moveTo( 0, 0 );
			cx.lineTo( 0, h );
			cx.lineTo( w, h );
			cx.lineTo( w, 0 );
			cx.lineTo( 0, 0 );
		}
		this._sketch( outline, transform, 0, false );
		cx.clip( );
	}

	this._renderGlint( glint.glint, transform );
	cx.restore( );
};

/*
| Fills a figure and draws its borders.
|
| ~glint:     the GlintFigure to draw
| ~transform: transforms everything by this
*/
def.proto._renderFigure =
	function( glint, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	const color = glint.color;
	const fill = glint.fill;
	const figure = glint.figure;
	const width = glint.width;
	const nogrid = glint.nogrid;
	const cx = this._cx;
	const sus = this.resolution.superSampling;

	if( fill )
	{
		cx.beginPath( );
		this._fill( fill, figure, transform, nogrid );
	}

	if( color )
	{
		cx.beginPath( );
		this._sketch( figure, transform, 0.5 * sus, nogrid );
		this._setStrokeStyle( color, glint.dashed, transform );
		cx.lineWidth = transform.d( width );
		cx.stroke( );
	}
};

/*
| Renders image data.
*/
def.proto._renderImageData =
	function( glint, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( glint.ti2ctype !== GlintImageData ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	const cx = this._cx;
	const pos = glint.pos;
	const x = this._x( pos.x, transform );
	const y = this._y( pos.y, transform );

	cx.putImageData( glint.data, x, y );
};

/*
| Renders a string using opentype.
|
| ~glint: the glint to render
| ~transform: transforms everything by this
*/
def.proto._renderString =
	function( glint, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( glint.ti2ctype !== GlintString ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	const cx = this._cx;
	const p = glint.p;

	// this shouldn't happen
/**/if( CHECK && transform.scale !== 1 ) throw new Error( );

	glint.fontString._drawOnCanvas(
		p.transform( transform ), glint.align, glint.base, this.resolution, cx
	);
};

/*
| Renders a transform change.
*/
def.proto._renderTransform =
	function( glint, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( glint.ti2ctype !== GlintTransform ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	this._renderGlint( glint.glint, transform.combine( glint.transform ) );
};

/*
| Renders a window.
*/
def.proto._renderWindow =
	function( glint, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( glint.ti2ctype !== GlintWindow ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	const cx = this._cx;
	const pane = glint.pane;
	const size = pane.size;
	const gpos = glint.pos;
	transform = transform.addOffset( gpos );
	const rw = size.width;
	const rh = size.height;
	const x = this._x( 0, transform );
	const y = this._y( 0, transform );
	const x2 = this._x( rw, transform );
	const y2 = this._y( rh, transform );

	// FIXME this isnt right
	if( x > x2 || y > y2 || x2 < 0 || y2 < 0 )
	{
		// if the window isn't visible at all
		// no need to render it.
		return;
	}

	if( rh * rw > glintCacheLimit )
	{
		cx.save( );
		cx.beginPath( );
		cx.moveTo( x, y );
		cx.lineTo( x2, y );
		cx.lineTo( x2, y2 );
		cx.lineTo( x, y2 );
		cx.lineTo( x, y );
		const bg = pane.background;
		if( bg )
		{
			cx.fillStyle = bg.css;
			cx.fill( );
		}
		cx.clip( );
		this._renderGlint( pane.glint, transform );
		cx.restore( );
	}
	else
	{
		const cv = pane._canvasDisplay._canvas;
		if( cv.width > 0 && cv.height > 0 )
		{
			cx.drawImage( cv, x, y );
		}
	}
};

/*
| Renders a text using opentype.
*/
def.proto._renderZoomGrid =
	function( glint, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( glint.ti2ctype !== GlintZoomGrid ) throw new Error();
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	const go = glint.offset;
	const spacing = glint.spacing;
	const grid = glint.grid;
	const xs = spacing.x;
	const ys = spacing.y;

	const colorH = glint.colorHeavy;
	const colorL = glint.colorLight;
	const cssH = colorH.css;

	const rH = colorH.red;
	const rL = colorL.red;
	const gH = colorH.green;
	const gL = colorL.green;
	const bH = colorH.blue;
	const bL = colorL.blue;

	let xw0 = false;
	let yw0 = false;
	let x0 = go.x;
	let y0 = go.y;

	while( x0 > xs )
	{
		xw0 = !xw0;
		x0 -= xs;
	}

	while( y0 > ys )
	{
		yw0 = !yw0;
		y0 -= ys;
	}

	const size = glint.size;
	const w = size.width;
	const h = size.height;
	const cx = this._cx;

	const rI = round( ( rL - rH ) * ( 2 - 2 * grid ) ) + rH;
	const gI = round( ( gL - gH ) * ( 2 - 2 * grid ) ) + gH;
	const bI = round( ( bL - bH ) * ( 2 - 2 * grid ) ) + bH;

	// intermediary color that blands into the background depending on zoom
	const cssI = 'rgb( ' + rI + ', ' + gI + ', ' + bI + ' )';

	const dp = transform.d( 2 );

	for( let x = x0, xw = xw0; x < w; x += xs, xw = !xw )
	{
		for( let y = y0, yw = yw0; y < h; y += ys, yw = !yw )
		{
			cx.fillStyle = xw || yw ? cssI : cssH;
			cx.fillRect( this._x( x, transform ), this._y( y, transform ), dp, dp );
		}
	}
};

/*
| Sets a HTML5 stroke style.
*/
def.proto._setFillStyle =
	function( style, figure, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	const cx = this._cx;

	// gradient
	let grad;

	switch( style.ti2ctype )
	{
		case Color:
			cx.fillStyle = style.css;
			return;

		case GradientAskew:
		{
/**/		if( CHECK && !figure.pos ) throw new Error( );

			grad =
				cx.createLinearGradient(
					this._x( figure.pos.x, transform ),
					this._y( figure.pos.y, transform ),
					this._x( figure.pos.x + figure.width / 10, transform ),
					this._y( figure.pos.y + figure.width, transform )
				);
			break;
		}

		case GradientRadial:
		{
			const r0 = figure.gradientR0 || 0;
			let r1 = figure.gradientR1;
			if( r1 === undefined )
			{
				r1 = Math.max( figure.width, figure.height );
			}
			const pc = figure.gradientPC || figure.pc;

/**/		if( CHECK )
/**/		{
/**/			// gradient misses gradient[PC|R0|R1]
/**/			if( pc.ti2ctype !== Point ) throw new Error( );
/**/			if( typeof( r0 ) !== 'number' ) throw new Error( );
/**/			if( typeof( r1 ) !== 'number' ) throw new Error( );
/**/		}

			grad =
				this._cx.createRadialGradient(
					this._x( pc.x, transform ),
					this._y( pc.y, transform ),
					this._d( r0 ),
					this._x( pc.x, transform ),
					this._y( pc.y, transform ),
					this._d( r1 )
				);
			break;
		}

		default: throw new Error( );
	}

	for( let cs of style )
	{
		grad.addColorStop( cs.offset, cs.color.css );
	}

	cx.fillStyle = grad;
};

/*
| Sets a HTML5 stroke style.
*/
def.proto._setStrokeStyle =
	function( color, dashed, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/	if( color.ti2ctype !== Color ) throw new Error( );
/**/	if( dashed !== undefined && dashed.ti2ctype !== Dashed ) throw new Error( );
/**/}

	const cx = this._cx;

	cx.strokeStyle = color.css;

	if( dashed )
	{
		cx.setLineDash( dashed._list );
		cx._dashed = true;
	}
	else
	{
		if( cx._dashed )
		{
			cx.setLineDash( [ ] );
			cx._dashed = false;
		}
	}
};


/*
| Sketches a bezier.
*/
def.proto._sketchBezier =
	function( bezier, transform, shift, nogrid )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 ) throw new Error( );
/**/	if( bezier.ti2ctype !== Bezier ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	const cx = this._cx;
	const p0 = bezier.p0;
	const p1 = bezier.p1;
	const pco0 = bezier.pco0;
	const pco1 = bezier.pco1;
	const x0 = this._x( p0.x, transform, nogrid ) + shift;
	const y0 = this._y( p0.y, transform, nogrid ) + shift;
	const x1 = this._x( p1.x, transform, nogrid ) + shift;
	const y1 = this._y( p1.y, transform, nogrid ) + shift;
	const xco0 = this._x( pco0.x, transform, nogrid ) + shift;
	const yco0 = this._y( pco0.y, transform, nogrid ) + shift;
	const xco1 = this._x( pco1.x, transform, nogrid ) + shift;
	const yco1 = this._y( pco1.y, transform, nogrid ) + shift;

	cx.moveTo( x0, y0 );
	cx.bezierCurveTo( xco0, yco0, xco1, yco1, x1, y1 );
};

/*
| Sketches a line.
*/
def.proto._sketchLine =
	function( line, transform, shift )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( line.ti2ctype !== Line ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	const cx = this._cx;
	const p0 = line.p0;
	const p1 = line.p1;
	const p0x = p0.x;
	const p0y = p0.y;
	const an = line.angle;

	if( !an.steep )
	{
		const xe = this.size.width;
		const t = an.sin / an.cos;
		const yw = p0y + p0x * t;
		const ye = p0y + ( p0x - xe ) * t;
		// FIXME also this looks very awkward
		cx.moveTo( shift, this._y( yw, transform ) + shift );
		cx.lineTo( this._x( xe, transform ) + shift, this._y( ye, transform ) + shift );
	}
	else
	{
		const ys = this.size.height;
		const at = an.cos / an.sin;
		const xn = p0x + p0y * at;
		const xs = p0x + ( p0y - ys ) * at;

		// FIXME why does sketch_line make 3 lines???
		cx.moveTo( this._x( xn, transform ) + shift, shift );
		cx.lineTo( this._x( p0.x, transform ) + shift, this._y( p0.y, transform ) + shift );
		cx.lineTo( this._x( p1.x, transform ) + shift, this._y( p1.y, transform ) + shift );
		cx.lineTo( this._x( xs, transform ) + shift, this._y( ys, transform ) + shift );
	}
};

/*
| Sketches a ray.
*/
def.proto._sketchRay =
	function( line, transform, shift, nogrid )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 ) throw new Error( );
/**/	if( line.ti2ctype !== Ray ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	const cx = this._cx;
	const p0 = line.p0;
	const p1 = line.p1;
	const p0x = p0.x;
	const p0y = p0.y;
	const p1x = p1.x;
	const p1y = p1.y;
	const an = line.angle;

	if( !an.steep )
	{
		const t = an.sin / an.cos;
		const xe = p1x >= p0x ? this.size.width : 0;
		const ye = p0y + ( p0x - xe ) * t;
		cx.moveTo(
			this._x( p0x, transform, nogrid ) + shift,
			this._y( p0y, transform, nogrid ) + shift,
		);

		cx.lineTo(
			this._x( xe, transform, nogrid ) + shift,
			this._y( ye, transform, nogrid ) + shift,
		);
	}
	else
	{
		const at = an.cos / an.sin;
		const ys = p1y >= p0y ? this.size.height : 0;
		const xs = p0x + ( p0y - ys ) * at;

		cx.moveTo(
			this._x( p0x, transform, nogrid ) + shift,
			this._y( p0y, transform, nogrid ) + shift,
		);

		cx.lineTo(
			this._x( xs, transform, nogrid ) + shift,
			this._y( ys, transform, nogrid ) + shift,
		);
	}
};

/*
| Sketches a line segment.
*/
def.proto._sketchSegment =
	function( segment, transform, shift, nogrid )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 ) throw new Error( );
/**/	if( segment.ti2ctype !== Segment ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	const cx = this._cx;
	const p0 = segment.p0;
	const p1 = segment.p1;

	cx.moveTo(
		this._x( p0.x, transform, nogrid ) + shift,
		this._y( p0.y, transform, nogrid ) + shift,
	);

	cx.lineTo(
		this._x( p1.x, transform, nogrid ) + shift,
		this._y( p1.y, transform, nogrid ) + shift,
	);
};

/*
| Sketches a qbend.
*/
def.proto._sketchQBend =
	function( qbend, transform, shift, nogrid )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 ) throw new Error( );
/**/	if( qbend.ti2ctype !== QBend ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	const cx = this._cx;
	const p0 = qbend.p0;
	const p1 = qbend.p1;

	const x1 = this._x( p0.x, transform, nogrid ) + shift;
	const y1 = this._y( p0.y, transform, nogrid ) + shift;
	const x2 = this._x( p1.x, transform, nogrid ) + shift;
	const y2 = this._y( p1.y, transform, nogrid ) + shift;

	let dx = x2 - x1;
	let dy = y2 - y1;
	let dxy = dx * dy;
	const magic = Ellipse.magic;
	cx.moveTo( x1, y1 );

	if( !qbend.ccw )
	{
		cx.bezierCurveTo(
			x1 + ( dxy > 0 ? magic * dx : 0 ),
			y1 + ( dxy < 0 ? magic * dy : 0 ),
			x2 - ( dxy < 0 ? magic * dx : 0 ),
			y2 - ( dxy > 0 ? magic * dy : 0 ),
			x2, y2
		);
	}
	else
	{
		cx.bezierCurveTo(
			x1 + ( dxy < 0 ? magic * dx : 0 ),
			y1 + ( dxy > 0 ? magic * dy : 0 ),
			x2 - ( dxy > 0 ? magic * dx : 0 ),
			y2 - ( dxy < 0 ? magic * dy : 0 ),
			x2, y2
		);
	}
};

/*
| Sketches a curve.
*/
def.proto._sketchQCurve =
	function( qcurve, transform, shift, nogrid )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 ) throw new Error( );
/**/	if( qcurve.ti2ctype !== QCurve ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	const cx = this._cx;
	const p0 = qcurve.p0;
	const p1 = qcurve.p1;
	const pco = qcurve.pco;

	const x0 = this._x( p0.x, transform, nogrid ) + shift;
	const y0 = this._y( p0.y, transform, nogrid ) + shift;
	const x1 = this._x( p1.x, transform, nogrid ) + shift;
	const y1 = this._y( p1.y, transform, nogrid ) + shift;
	const xco = this._x( pco.x, transform, nogrid ) + shift;
	const yco = this._y( pco.y, transform, nogrid ) + shift;

	cx.moveTo( x0, y0 );
	cx.quadraticCurveTo( xco, yco, x1, y1 );
};

/*
| Sketches a rectangle.
*/
def.proto._sketchRect =
	function( rect, transform, shift, nogrid )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 ) throw new Error( );
/**/	if( rect.ti2ctype !== Rect ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/	if( typeof( shift ) !== 'number' ) throw new Error( );
/**/}

	const cx = this._cx;
	const pos = rect.pos;
	const wx = this._x( pos.x, transform, nogrid ) + shift;
	const ny = this._y( pos.y, transform, nogrid ) + shift;
	const ex = this._x( pos.x + rect.width, transform, nogrid ) + shift;
	const sy = this._y( pos.y + rect.height, transform, nogrid ) + shift;

	cx.moveTo( wx, ny );
	cx.lineTo( ex, ny );
	cx.lineTo( ex, sy );
	cx.lineTo( wx, sy );
	cx.lineTo( wx, ny );
};

/*
| Sketches a figure.
|
| ~figure:    figure to sketch
| ~transform: transform
| ~shift:     pixel shift
| ~nogrid:    opt out of pixel grid fitting
*/
def.proto._sketch =
	function( figure, transform, shift, nogrid )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	// FIXME make a map
	switch( figure.ti2ctype )
	{
		case Arrow:
		case Ellipse:
		case Pin:
		case RectRound:
		case RectRoundExt:
		case RingSector:
			this._sketchPath( figure.path, transform, shift, nogrid );
			return;

		case Bezier:
			this._sketchBezier( figure, transform, shift, nogrid );
			return;

		case FigureList:
			for( let f of figure )
			{
				this._sketch( f, transform, shift, nogrid );
			}
			return;

		case Line:
			this._sketchLine( figure, transform, shift, nogrid );
			return;

		case QBend:
			this._sketchQBend( figure, transform, shift, nogrid );
			return;

		case QCurve:
			this._sketchQCurve( figure, transform, shift, nogrid );
			return;

		case Path:
			this._sketchPath( figure, transform, shift, nogrid );
			return;

		case Ray:
			this._sketchRay( figure, transform, shift, nogrid );
			return;

		case Rect:
			this._sketchRect( figure, transform, shift, nogrid );
			return;

		case Segment:
			this._sketchSegment( figure, transform, shift, nogrid );
			return;

		default: throw new Error( );
	}
};

/*
| Sketches a path.
|
| ~path:      path to sketch
| ~transform: transform
| ~shift:     pixel shift
| ~nogrid:    opt out of pixel grid fitting
*/
def.proto._sketchPath =
	function( path, transform, shift, nogrid )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 ) throw new Error( );
/**/	if( path.ti2ctype !== Path ) throw new Error( );
/**/	if( !TransformBase.isa( transform ) ) throw new Error( );
/**/}

	const cx = this._cx;
	const magic = Ellipse.magic;
	const parts = path.parts;
	let part = parts.get( 0 );
	let p = part.p0;
	// start point x/y
	let psx, psy;
	psx = this._x( p.x, transform, nogrid ) + shift;
	psy = this._y( p.y, transform, nogrid ) + shift;
	const al = parts.length;
	// current point x/y
	let cpx = psx, cpy = psy;
	// next point
	let pnx, pny;

	cx.moveTo( psx, psy );
	let nextPart = parts.get( 0 );
	for( let a = 0; a < al; a++ )
	{
		part = nextPart;
		nextPart = a + 1 < al ? parts.get( a + 1 ) : parts.get( 0 );
		p = part.p1;

		pnx = this._x( p.x, transform, nogrid ) + shift;
		pny = this._y( p.y, transform, nogrid ) + shift;

		switch( part.ti2ctype )
		{
			case Bezier:
			{
				const pco0 = part.pco0;
				const pco1 = part.pco1;
				const pco0x = this._x( pco0.x, transform, nogrid ) + shift;
				const pco0y = this._y( pco0.y, transform, nogrid ) + shift;
				const pco1x = this._x( pco1.x, transform, nogrid ) + shift;
				const pco1y = this._y( pco1.y, transform, nogrid ) + shift;
				cx.bezierCurveTo( pco0x, pco0y, pco1x, pco1y, pnx, pny );
				break;
			}

			case Blind:
				// ignore
				break;

			case QBend:
			{
				if( shift && nextPart.ti2ctype === QBend )
				{
					// workaround gap bug in chrome
					// FIXME needed?
					pnx += 0.1;
					pny += 0.1;
				}
				let dx = pnx - cpx;
				let dy = pny - cpy;
				let dxy = dx * dy;
				if( !part.ccw )
				{
					cx.bezierCurveTo(
						cpx + ( dxy > 0 ? magic * dx : 0 ),
						cpy + ( dxy < 0 ? magic * dy : 0 ),
						pnx - ( dxy < 0 ? magic * dx : 0 ),
						pny - ( dxy > 0 ? magic * dy : 0 ),
						pnx, pny
					);
				}
				else
				{
					cx.bezierCurveTo(
						cpx + ( dxy < 0 ? magic * dx : 0 ),
						cpy + ( dxy > 0 ? magic * dy : 0 ),
						pnx - ( dxy > 0 ? magic * dx : 0 ),
						pny - ( dxy < 0 ? magic * dy : 0 ),
						pnx, pny
					);
				}
				break;
			}

			case QCurve:
			{
				const pco = part.pco;
				const pcox = this._x( pco.x, transform, nogrid ) + shift;
				const pcoy = this._y( pco.y, transform, nogrid ) + shift;
				cx.quadraticCurveTo( pcox, pcoy, pnx, pny );
				break;
			}

			case Segment:
				if( !part.fly || shift === 0 )
				{
					cx.lineTo( pnx, pny );
				}
				else
				{
					cx.moveTo( pnx, pny );
				}
				break;

			// unknown hull section.
			default: throw new Error( );
		}

		cpx = pnx;
		cpy = pny;
	}

	if( path.closed )
	{
		cx.closePath( );
	}
};

/*
| Transforms a x-value and rounds it on the pixel grid.
|
| ~x:         the value to transform
| ~transform: the transform to apply
| ~nogrid:    don't round it to the pixel grid.
*/
def.proto._x =
	function( x, transform, nogrid )
{
	x = transform.x( x );

	if( !nogrid )
	{
		return this.resolution.fit( x );
	}
	else
	{
		return x;
	}
};

/*
| Transforms a y-value and rounds it on the pixel grid.
|
| ~y:         the value to transform
| ~transform: the transform to apply
| ~nogrid:    don't round it to the
*/
def.proto._y =
	function( y, transform, nogrid )
{
	y = transform.y( y );

	if( !nogrid )
	{
		return this.resolution.fit( y );
	}
	else
	{
		return y;
	}
};
