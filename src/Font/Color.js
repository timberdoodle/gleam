/*
| A font with a specific size and color.
*/
def.attributes =
{
	// color of the font
	color: { type: 'Color/Self', json: true },

	// colorless font family and size
	fontBland: { type: 'Font/Bland', json: true },
};

def.json = true;

import { Self as FontStringNormal } from '{Font/String/Normal}';
import { Self as FontStringRotate } from '{Font/String/Rotate}';

/*
| Resizes this font by factor and rounds the result.
*/
def.proto.RoundResize =
	function( factor )
{
	if( factor === 1 )
	{
		return this.roundSize;
	}

	let fb = this.fontBland;
	fb = fb.family.Size( Math.round( fb.size * factor ) );
	return fb.FontColor( this.color );
};

/*
| Returns the fontColor with rounded size.
*/
def.lazy.RoundSize =
	function( )
{
	return this.fontBland.roundSize.FontColor( this.color );
};

/*
| Shortcut to the size of the font.
*/
def.lazy.size =
	function( )
{
	return this.fontBland.size;
};

/*
| Creates a string.
*/
def.proto.StringNormal =
	function( string )
{
	return FontStringNormal.FontColorString( this, string );
};

/*
| Creates a rotated string.
*/
def.proto.StringRotate =
	function( string, rotation )
{
	return FontStringRotate.FontColorRotationString( this, rotation, string );
};

/*
| Applies a transformation scale.
*/
def.proto.transform =
	function( transform )
{
	return this.fontBland.transform( transform ).FontColor( this.color );
};
