/*
| Masked glints.
*/
def.attributes =
{
	// the glints to draw
	glint:
	{
		type:
		[
			'list@<Glint/Types',
			'Glint/Figure',
			'Glint/Transform',
			'Glint/Window',
		],
		json: true,
	},

	// name of the clint for debugging & testsuite
	name: { type: [ 'undefined', 'string' ], json: true },

	// the outline to mask
	outline: { type: [ '< Figure/Types' ], json: true },

	// true if reversing mask
	reversed: { type: [ 'undefined', 'boolean' ], json: true },
};

def.json = true;
