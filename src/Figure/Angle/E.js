/*
| East, 0°
*/
def.extend = 'Figure/Angle/Base';
def.singleton = true;

def.json = true;

import { Self as Angle } from '{Figure/Angle/Self}';

/*
| One intermediate cardinal step counter clockwise.
*/
def.lazy.ccw08 = ( ) => Angle.ne;

/*
| Cosinus value of this angle.
*/
def.proto.cos = 1;

/*
| One intermediate cardinal step clockwise.
*/
def.lazy.cw08 = ( ) => Angle.se;

/*
| Gets point from a rect.
*/
def.proto.from = ( rect ) => rect.pe;

/*
| Has x-component.
*/
def.proto.hasX = true;

/*
| Has no y component.
*/
def.proto.hasY = false;

/*
| Opposite direction.
*/
def.lazy.opposite = ( ) => Angle.w;

/*
| Radians constant.
*/
def.proto.radians = 0;

/*
| Sinus value of this angle.
*/
def.proto.sin = 0;

/*
| True if the angle is nearer to the y-axis than the x-axis.
*/
def.proto.steep = false;
