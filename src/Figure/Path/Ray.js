/*
| A ray, one sided open, infinitely long.
|
| See also Line and Segment.
*/
def.attributes =
{
	p0: { type: 'Figure/Point', json: true },
	p1: { type: 'Figure/Point', json: true },
};

def.json = true;

const abs = Math.abs;
const atan2 = Math.atan2;
const sqrt = Math.sqrt;

import { Self as Angle     } from '{Figure/Angle/Self}';
import { Self as Ellipse   } from '{Figure/Shape/Ellipse}';
import { Self as Line      } from '{Figure/Path/Line}';
import { Self as Point     } from '{Figure/Point}';
import { Self as Rect      } from '{Figure/Shape/Rect}';
import { Self as RectRound } from '{Figure/Shape/RectRound}';
import { Self as QBend     } from '{Figure/Path/QBend}';

/*
| Moves the line by p or x/y.
*/
def.proto.add =
	function( /* p or x,y */ )
{
	const p0 = this.p0;
	const p1 = this.p1;
	return(
		Self.P0P1(
			p0.add.apply( p0, arguments ),
			p1.add.apply( p1, arguments )
		)
	);
};

/*
| Returns the angle of this ray.
*/
def.lazy.angle =
	function( )
{
	const p0 = this.p0;
	const p1 = this.p1;
	let r = atan2( p0.y - p1.y, p1.x - p0.x );
	return Angle.Radians( r );
};

/*
| Returns the distance of a point to the line segment.
*/
def.proto.distanceOfPoint =
	function( /* p or x/y */ )
{
	return abs( this.signedDistanceOfPoint.apply( this, arguments ) );
};

/*
| Creates a Line by p0 and angle
*/
def.static.P0Angle =
	function( p0, angle )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( p0.ti2ctype !== Point ) throw new Error( );
/**/}

	const p1 =
		Point.XY(
			p0.x + angle.cos * 100,
			p0.y - angle.sin * 100
		);

	// FIXME ahead the angle (same in Segment)
	return Self.P0P1( p0, p1 );
};

/*
| Intersects this line segment with another figure.
|
| ~figure: figure to intersect with
*/
def.proto.intersectsPoint =
	function( figure )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	if(
		figure.ti2ctype === Ellipse
		|| figure.ti2ctype === QBend
		|| figure.ti2ctype === Rect
		|| figure.ti2ctype === RectRound
	)
	{
		return figure.intersectsPoint( this );
	}

	throw new Error( );
};

/*
| Returns >0 if p is left of this line. <0 if right, 0 if on.
*/
def.proto.sideOfPoint =
	function( p )
{
	const p0 = this.p0;
	const p1 = this.p1;
	return ( p1.x - p0.x )*( p.y - p0.y ) - ( p.x - p0.x )*( p1.y - p0.y );
};

/*
| Slope of line.
*/
def.lazy.k =
	function( )
{
	const p0 = this.p0;
	const p1 = this.p1;
	const dx = p1.x - p0.x;
	const dy = p1.y - p0.y;
	return dy / dx;
};

/*
| Creation shortcut.
*/
def.static.P0P1 = ( p0, p1 ) => Self.create( 'p0', p0, 'p1', p1 );

/*
| Returns the distance of a point to the line segment.
*/
def.proto.signedDistanceOfPoint =
	function( a1, a2 /* p or x/y */ )
{
	let x, y;
	if( arguments.length === 1 ) { x = a1.x; y = a1.y; }
	else { x = a1; y = a2; }

	const x0 = this.p0.x;
	const y0 = this.p0.y;
	const x1 = this.p1.x;
	const y1 = this.p1.y;

	const dy = y1 - y0;
	const dx = x1 - x0;
	const dn = sqrt( dx*dx + dy*dy );
	const no = dy*x - dx*y + x1*y0 - y1*x0;

	return no / dn;
};

/*
| Returns a transformed line segment.
*/
def.proto.transform =
	function( transform )
{
	return(
		this.create(
			'p0', this.p0.transform( transform ),
			'p1', this.p1.transform( transform )
		)
	);
};

/*
| Returns true if a point (x/y) is within the limits of the path,
| assuming it is on the path.
|
| Always true for lines.
| True for segments if within segment.
| True for rays if on the ray side.
|
| ~x/y, or p: point to test
*/
def.proto.withinLimits =
	function( a1, a2 )
{
	let x, y;
	if( arguments.length === 1 ) { x = a1.x; y = a1.y; }
	else{ x = a1; y = a2; }

	const p0 = this.p0;
	const p1 = this.p1;
	const x0 = p0.x;
	const x1 = p1.x;

	if( x0 !== x1 )
	{
		return x1 >= x0 ? x >= x0 : x >= x1;
	}
	else
	{
		const y0 = p0.y;
		const y1 = p1.y;
		return y1 >= y0 ? y >= y0 : y >= y1;
	}
};

/*
| The zone of the line segment.
*/
def.lazy.zone =
	function( )
{
	return Rect.Arbitrary( this.p0, this.p1 );
};

/*
| Intersects this line segment with a line.
|
| ~line:     the line to intersect with
*/
def.proto._intersectsLinePoint =
	function( line )
{
/**/if( CHECK && line.ti2ctype !== Line ) throw new Error( );

	const p0 = this.p0;
	const p1 = this.p1;
	const p2 = line.p0;
	const p3 = line.p1;

	const x0 = p0.x, y0 = p0.y;
	const x1 = p1.x, y1 = p1.y;
	const x2 = p2.x, y2 = p2.y;
	const x3 = p3.x, y3 = p3.y;

	if( x0 === x2 && y0 === y2 ) return p0;
	if( x0 === x3 && y0 === y3 ) return p0;
	if( x1 === x2 && y1 === y2 ) return p1;
	if( x1 === x3 && y1 === y3 ) return p1;

	const den = ( x0 - x1 )*( y2 - y3 ) - ( y0 - y1 )*( x2 - x3 );
	if( den === 0 )
	{
		// doesn't intersect
		return undefined;
	}
	const a = x0*y1 - y0*x1;
	const b = x2*y3 - y2*x3;

	const x = ( a*( x2 - x3 ) - ( x0 - x1 )*b ) / den;
	const y = ( a*( y2 - y3 ) - ( y0 - y1 )*b ) / den;

	// checks if the intersection is on the segment
	return(
		this.withinLimits( x, y )
		? Point.XY( x, y )
		: undefined
	);
};

/*
| Intersects this line segment with a ray.
*/
def.proto._intersectsRayPoint =
	function( ray )
{
/**/if( CHECK && ray.ti2ctype !== Self ) throw new Error( );

	const tp0 = this.p0;
	const tp1 = this.p1;
	const rp0 = ray.p0;
	const rp1 = ray.p1;

	const t0x = tp0.x, t0y = tp0.y;
	const t1x = tp1.x, t1y = tp1.y;
	const r0x = rp0.x, r0y = rp0.y;
	const r1x = rp1.x, r1y = rp1.y;

	if( t0x === r0x && t0y === r0y ) return tp0;
	if( t0x === r1x && t0y === r1y ) return tp0;
	if( t1x === r0x && t1y === r0y ) return tp1;
	if( t1x === r1x && t1y === r1y ) return tp1;

	const den = ( t0x - t1x )*( r0y - r1y ) - ( t0y - t1y )*( r0x - r1x );
	if( den === 0 )
	{
		// doesn't intersect
		return undefined;
	}
	const a = t0x * t1y - t0y * t1x;
	const b = r0x * r1y - r0y * r1x;

	let x, y;

	// in case one line is vertical the intersection must be on that x value
	if( t0x === t1x ) x = t0x;
	else if( r0x === r1x ) x = r0x;
	else x = ( a*( r0x - r1x ) - ( t0x - t1x )*b ) / den;

	// in case one line is horizontal the intersection must be on that y value
	if( t0y === t1y ) y = t0y;
	else if( r0y === r1y ) y = r0y;
	else y = ( a*( r0y - r1y ) - ( t0y - t1y )*b ) / den;

	// checks if the intersection is on the rays.
	return(
		Self.withinLimits( x, y ) && ray.withinLimits( x, y )
		? Point.XY( x, y )
		: undefined
	);
};

/*
| Returns the intersections of the segment with anoter.
|
| ~segment:     the figure to find intersections with
| ~extended:    if defined figures are to be extended beyond their end.
| ~flip:        if defined flips results
| ~resultArray: if defined push to this
*/
def.proto._intersectsSegmentAt =
	function( segment, extended, flip, resultArray )
{
/**/if( CHECK && segment.ti2ctype !== Self ) throw new Error( );

	const p0 = this.p0;
	const p1 = this.p1;
	const p2 = segment.p0;
	const p3 = segment.p1;

	const x0 = p0.x, y0 = p0.y;
	const x1 = p1.x, y1 = p1.y;
	const x2 = p2.x, y2 = p2.y;
	const x3 = p3.x, y3 = p3.y;

	const den = ( x0 - x1 )*( y2 - y3 ) - ( y0 - y1 )*( x2 - x3 );
	if( den === 0 ) return resultArray; // doesn't intersect
	const a = x0*y1 - y0*x1;
	const b = x2*y3 - y2*x3;

	const x = ( a*( x2 - x3 ) - ( x0 - x1 )*b ) / den;
	const y = ( a*( y2 - y3 ) - ( y0 - y1 )*b ) / den;

	let at0, at1;
	let dx = x0 - x1, dy = y0 - y1;

	if( abs( dx ) > abs( dy ) ) at0 = ( x0 - x ) / ( x0 - x1 );
	else at0 = ( y0 - y ) / ( y0 - y1 );

	dx = x2 - x3; dy = y2 - y3;
	if( abs( dx ) > abs( dy ) ) at1 = ( x2 - x ) / ( x2 - x3 );
	else at1 = ( y2 - y ) / ( y2 - y3 );

	if( !extended && ( at0 < 0 || at0 > 1 || at1 < 0 || at1 > 1 ) )
	{
		return resultArray;
	}

	if( !resultArray ) resultArray = [ ];

	if( !flip ) resultArray.push( at0, at1 );
	else resultArray.push( at1, at0 );

	return resultArray;
};

/*
| Intersects this line segment with another line Segment.
*/
def.proto._intersectsSegmentPoint =
	function( segment )
{
/**/if( CHECK && segment.ti2ctype !== Self ) throw new Error( );

	const tp0 = this.p0;
	const tp1 = this.p1;
	const sp0 = segment.p0;
	const sp1 = segment.p1;

	const t0x = tp0.x, t0y = tp0.y;
	const t1x = tp1.x, t1y = tp1.y;
	const s0x = sp0.x, s0y = sp0.y;
	const s1x = sp1.x, s1y = sp1.y;

	if( t0x === s0x && t0y === s0y ) return tp0;
	if( t0x === s1x && t0y === s1y ) return tp0;
	if( t1x === s0x && t1y === s0y ) return tp1;
	if( t1x === s1x && t1y === s1y ) return tp1;

	const den = ( t0x - t1x )*( s0y - s1y ) - ( t0y - t1y )*( s0x - s1x );
	if( den === 0 )
	{
		// doesn't intersect
		return undefined;
	}
	const a = t0x * t1y - t0y * t1x;
	const b = s0x * s1y - s0y * s1x;

	let x, y;

	// in case one line is vertical the intersection must be on that x value
	if( t0x === t1x ) x = t0x;
	else if( s0x === s1x ) x = s0x;
	else x = ( a*( s0x - s1x ) - ( t0x - t1x )*b ) / den;

	// in case one line is horizontal the intersection must be on that y value
	if( t0y === t1y ) y = t0y;
	else if( s0y === s1y ) y = s0y;
	else y = ( a*( s0y - s1y ) - ( t0y - t1y )*b ) / den;

	// checks if the intersection is on the segments.
	return(
		this.withinLimits( x, y ) && segment.withinLimits( x, y )
		? Point.XY( x, y )
		: undefined
	);
};
