/*
| A line (that is infinitely long, for finite see Segment).
*/
def.attributes =
{
	p0: { type: 'Figure/Point', json: true },
	p1: { type: 'Figure/Point', json: true },
};

def.json = true;

const abs = Math.abs;
const atan2 = Math.atan2;
const sqrt = Math.sqrt;

import { Self as Angle } from '{Figure/Angle/Self}';
import { Self as Point } from '{Figure/Point}';
import { Self as QBend } from '{Figure/Path/QBend}';

/*
| Moves the line by p or x/y.
*/
def.proto.add =
	function( /* p or x,y */ )
{
	const p0 = this.p0;
	const p1 = this.p1;
	return(
		Self.P0P1(
			p0.add.apply( p0, arguments ),
			p1.add.apply( p1, arguments )
		)
	);
};

/*
| Returns the angle of this line.
*/
def.lazy.angle =
	function( )
{
	const p0 = this.p0;
	const p1 = this.p1;
	let r = atan2( p0.y - p1.y, p1.x - p0.x );
	return Angle.Radians( r );
};

/*
| Returns the distance of a point to the line segment.
*/
def.proto.distanceOfPoint =
	function( /* p or x/y */ )
{
	return abs( this.signedDistanceOfPoint.apply( this, arguments ) );
};

/*
| Intersects this line segment with another figure.
|
| ~figure:   FIXME
*/
def.proto.intersectsPoint =
	function( figure )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	if( figure.ti2ctype === QBend ) return figure.intersectsPoint( this );

/**/if( CHECK && figure.ti2ctype !== Self ) throw new Error( );

	const p0 = this.p0;
	const p1 = this.p1;
	const p2 = figure.p0;
	const p3 = figure.p1;

	const x0 = p0.x, y0 = p0.y;
	const x1 = p1.x, y1 = p1.y;
	const x2 = p2.x, y2 = p2.y;
	const x3 = p3.x, y3 = p3.y;

	if( x0 === x2 && y0 === y2 ) return p0;
	if( x0 === x3 && y0 === y3 ) return p0;
	if( x1 === x2 && y1 === y2 ) return p1;
	if( x1 === x3 && y1 === y3 ) return p1;

	const den = ( x0 - x1 )*( y2 - y3 ) - ( y0 - y1 )*( x2 - x3 );
	if( den === 0 )
	{
		// doesn't intersect
		return undefined;
	}
	const a = x0*y1 - y0*x1;
	const b = x2*y3 - y2*x3;

	let x, y;
	x = ( a*( x2 - x3 ) - ( x0 - x1 )*b ) / den;
	y = ( a*( y2 - y3 ) - ( y0 - y1 )*b ) / den;

	return Point.XY( x, y );
};

/*
| Slope of line.
*/
def.lazy.k =
	function( )
{
	const p0 = this.p0;
	const p1 = this.p1;
	const dx = p1.x - p0.x;
	const dy = p1.y - p0.y;
	return dy / dx;
};

/*
| Creates a Line by p0 and angle
*/
def.static.P0Angle =
	function( p0, angle )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( p0.ti2ctype !== Point ) throw new Error( );
/**/}

	const p1 =
		Point.XY(
			p0.x + angle.cos * 100,
			p0.y - angle.sin * 100
		);

	// FIXME ahead the angle (same in Segment)
	return Self.P0P1( p0, p1 );
};

/*
| Creation shortcut.
*/
def.static.P0P1 = ( p0, p1 ) => Self.create( 'p0', p0, 'p1', p1 );

/*
| Returns a parallel with (signed) distance d.
*/
def.proto.parallel =
	function( d )
{
	const angle = this.angle;
	let p0 = this.p0;
	let p1 = this.p1;

	p0 = p0.sub( d * angle.sin, d * angle.cos );
	p1 = p1.sub( d * angle.sin, d * angle.cos );

	return this.create( 'p0', p0, 'p1', p1 );
};

/*
| Returns the distance of a point to the line segment.
*/
def.proto.signedDistanceOfPoint =
	function( a1, a2 /* p or x/y */ )
{
	let x, y;
	if( arguments.length === 1 ) { x = a1.x; y = a1.y; }
	else { x = a1; y = a2; }

	const x0 = this.p0.x;
	const y0 = this.p0.y;
	const x1 = this.p1.x;
	const y1 = this.p1.y;

	const dy = y1 - y0;
	const dx = x1 - x0;
	const dn = sqrt( dx*dx + dy*dy );
	const no = dy*x - dx*y + x1*y0 - y1*x0;

	return no / dn;
};

/*
| Returns >0 if p is left of this line. <0 if right, 0 if on.
*/
def.proto.sideOfPoint =
	function( p )
{
	const p0 = this.p0;
	const p1 = this.p1;
	return ( p1.x - p0.x )*( p.y - p0.y ) - ( p.x - p0.x )*( p1.y - p0.y );
};

/*
| Returns a transformed line segment.
*/
def.proto.transform =
	function( transform )
{
	return(
		this.create(
			'p0', this.p0.transform( transform ),
			'p1', this.p1.transform( transform )
		)
	);
};

/*
| Returns true if a point (x/y) is within the limits of the path,
| assuming it is on the path.
|
| Always true for lines.
| True for segments if within segment.
| True for rays if on the ray side.
|
| ~x/y, or p: point to test
*/
def.proto.withinLimits = function( ) { return true; };
