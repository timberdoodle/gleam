/*
| A gradient color stop.
*/
def.attributes =
{
	// color stop offset ( from 0 to 1 )
	offset: { type: 'number', json: true },

	// color at stop
	color: { type: 'Color/Self', json: true }
};

def.json = true;

/*
| Shortcut
*/
def.static.OffsetColor =
	( offset, color ) =>
	Self.create( 'offset', offset, 'color', color );
