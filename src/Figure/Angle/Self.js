/*
| Angle root.
*/
//def.extend = 'Figure/Angle/Base';
def.abstract = true;

import { Self as Generic } from '{Figure/Angle/Generic}';
import { Self as N       } from '{Figure/Angle/N}';
import { Self as NE      } from '{Figure/Angle/Ne}';
import { Self as NW      } from '{Figure/Angle/Nw}';
import { Self as E       } from '{Figure/Angle/E}';
import { Self as S       } from '{Figure/Angle/S}';
import { Self as SE      } from '{Figure/Angle/Se}';
import { Self as SW      } from '{Figure/Angle/Sw}';
import { Self as W       } from '{Figure/Angle/W}';

const atan2 = Math.atan2;
const pi = Math.PI;
const pi2 = 2 * pi;

/*
| Returns the (intermidiate) cardianal direction by
| index.
*/
def.static.iDirByIndex =
	( idx ) =>
{
	const g = Self._iDirIndex[ idx ];

/**/if( CHECK && !g ) throw new Error( );

	return g;
};

/*
| Creates an angle from p0 to p1.
*/
def.static.P0P1 =
	function( p0, p1 )
{
	return Self.Radians( atan2( p0.y - p1.y, p1.x - p0.x ) );
};

/*
| Creates an angle by radians value.
*/
def.static.Radians =
	( radians ) =>
{
	radians = Self.restrict( radians );
	const idir = Self._angleMap.get( radians );

	return(
		idir
		? idir
		: Generic.create( 'radians', radians )
	);
};

/*
| North-west.
*/
def.staticLazy.nw =
	( ) =>
	NW.singleton;

/*
| North.
*/
def.staticLazy.n =
	( ) =>
	N.singleton;

/*
| North-east.
*/
def.staticLazy.ne =
	( ) =>
	NE.singleton;

/*
| East.
*/
def.staticLazy.e =
	( ) =>
	E.singleton;

/*
| South-east.
*/
def.staticLazy.se =
	( ) =>
	SE.singleton;

/*
| South.
*/
def.staticLazy.s =
	( ) =>
	S.singleton;

/*
| South-west.
*/
def.staticLazy.sw =
	( ) =>
	SW.singleton;

/*
| West.
*/
def.staticLazy.w =
	( ) =>
	W.singleton;

/*
| Returns a radians value for r that is from 0 to 2*pi.
*/
def.static.restrict =
	function( r )
{
	return(
		r >= 0
		? r % pi2
		: pi2 + ( r % pi2 )
	);
};

def.staticLazy._angleMap =
	( ) =>
{
	const map = new Map( );
	map.set( 0,          E.singleton  );
	map.set( pi / 4,     NE.singleton );
	map.set( pi / 2,     N.singleton  );
	map.set( pi * 3 / 4, NW.singleton );
	map.set( pi,         W.singleton  );
	map.set( pi * 5 / 4, SW.singleton );
	map.set( pi * 3 / 2, S.singleton  );
	map.set( pi * 7 / 4, SE.singleton );
	return map;
};

/*
| (intermidate) cardinal directions by index.
*/
def.staticLazy._iDirIndex =
	( ) =>
[
	E .singleton,
	NE.singleton,
	N .singleton,
	NW.singleton,
	W .singleton,
	SW.singleton,
	S .singleton,
	SE.singleton,
	E .singleton,
];
