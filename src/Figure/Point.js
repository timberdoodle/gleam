/*
| A point in a 2D plane.
*/
def.attributes =
{
	// x-coordinate
	x: { type: 'number', json: true },

	// y-coordinate
	y: { type: 'number', json: true },
};

def.json = true;

import { Self as Angle } from '{Figure/Angle/Self}';
import { Self as TransformBase } from '{Transform/Base}';

const round = Math.round;
const sqrt = Math.sqrt;

/*
| Moves the point by another point or x/y values, returns a new point.
*/
def.proto.add =
	function( a1, a2 )
{
	if( typeof( a1 ) === 'object' )
	{
/**/	if( CHECK &&  a2 !== undefined ) throw new Error( );
		return Self.XY( this.x + a1.x, this.y + a1.y );
	}
	else
	{
/**/	if( CHECK && arguments.length !== 2 ) throw new Error( );
		return Self.XY( this.x + a1, this.y + a2 );
	}
};

/*
| Returns the angle direction (n,s,e,w,ne,se,sw,nw) to
| another point;
*/
def.proto.angleDirTo =
	function( p )
{
/**/if( CHECK && p.ti2ctype !== Self ) throw new Error( );

	const tx = this.x;
	const ty = this.y;
	const px = p.x;
	const py = p.y;

	if( px === tx )
	{
		if( py === ty ) return undefined;
		else if( py > ty ) return Angle.s;
		else return Angle.n;
	}

	if( py === ty )
	{
		if( px > tx ) return Angle.e;
		else return Angle.w;
	}

	if( py < ty )
	{
		if( px > tx ) return Angle.ne;
		else return Angle.nw;
	}

	if( px > tx ) return Angle.se;
	else return Angle.sw;
};

/*
| Returns a point scaled by action.scaleX, action.scaleY
| relative to the action.bPoint.
| ax / ay are added afterward.
|
| ~action: action that scales the point
| ~ax:     x value to be added
| ~ay:     y value to be added
*/
def.proto.baseScaleAction =
	function( action, ax, ay )
{
	return this.baseScaleXY( action.scaleX, action.scaleY, action.pBase, ax, ay );
};

/*
| Returns a point scaled by scaleX, scaleY
| relative to the action.bPoint.
| ax / ay are added afterward.
|
| scaleX: x scaling
| scaleY: y scaling
| pBase:  base point
| ax:     x value to be added
| ay:     y value to be added
*/
def.proto.baseScaleXY =
	function( scaleX, scaleY, pBase, ax, ay )
{
	if( scaleX === 1 && scaleY === 1 ) return this.add( ax, ay );
	const x = this.x;
	const y = this.y;
	const bx = pBase.x;
	const by = pBase.y;
	if( x === bx && y === by ) return this.add( ax, ay );
	return(
		Self.XY(
			( x - bx ) * scaleX + bx + ax,
			( y - by ) * scaleY + by + ay
		)
	);
};

/*
| Returns a detransformed point.
| ~transform: transform to apply reversely.
*/
def.proto.detransform =
	function( transform )
{
	return Self.XY( transform.dex( this.x ), transform.dey( this.y ) );
};

/*
| Distance to another pointer
*/
def.proto.distanceOfPoint =
	function( p )
{
	const dx = this.x - p.x;
	const dy = this.y - p.y;

	return sqrt( dx * dx + dy * dy );
};

/*
| Shortcut to create a point from event on an HTML Element.
| ~event:      event to create from
| ~element:    offset of the element
| ~resolution: resolution
*/
def.static.FromEvent =
	function( event, element, resolution )
{
	const ratio = resolution.devicePixelRatio;
	return(
		Self.XY(
			( event.pageX - element.offsetLeft ) * ratio,
			( event.pageY - element.offsetTop ) * ratio
		)
	);
};

/*
| Rounds the point x/y values.
*/
def.lazy.round =
	function( )
{
	return Self.XY( round( this.x ), round( this.y ) );
};

/*
| Subtracts a point (or x/y from this), returns new point.
*/
def.proto.sub =
	function( a1, a2 )
{
	if( typeof( a1 ) === 'object' )
	{
/**/	if( CHECK && a2 !== undefined ) throw new Error( );
		return Self.XY( this.x - a1.x, this.y - a1.y );
	}
	else
	{
/**/	if( CHECK && arguments.length !== 2 ) throw new Error( );
		return Self.XY( this.x - a1, this.y - a2 );
	}
};

/*
| This point negated.
*/
def.lazy.negate =
	function( )
{
	const p = Self.XY( -this.x, -this.y );
	ti2c.aheadValue( p, 'negate', this );
	return p;
};

/*
| Returns a transformed point.
|
| ~transform: transform to apply
*/
def.proto.transform =
	function( transform )
{
/**/if( CHECK && !TransformBase.isa( transform ) ) throw new Error( );

	return Self.XY( transform.x( this.x ), transform.y( this.y ) );
};

/*
| Shortcut to create a point by x/y values.
*/
def.static.XY = ( x, y ) => Self.create( 'x', x, 'y', y );

/*
| Shortcut for point at 0/0.
*/
def.staticLazy.zero = ( ) => Self.XY( 0, 0 );
