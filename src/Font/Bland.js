/*
| A font with a specific size.
*/
def.attributes =
{
	// size of the font
	size: { type: 'number', json: true },

	// family of the font
	family: { type: 'Font/Family', json: true },
};

def.json = true;

import { Self as FontColor } from '{Font/Color}';
import { Self as FontStringBland } from '{Font/String/Bland}';

const opentypeOptions =
	Object.freeze( {
		kerning: true,
		features: { liga: true, rlig: true },
		script: 'latn'
	} );

const opentypeOptionsHinting =
	Object.freeze( {
		hinting: true,
		kerning: true,
		features: { liga: true, rlig: true },
		script: 'latn'
	} );

/*
| Creates a font color (a font with family, size and color)
*/
def.proto.FontColor =
	function( color )
{
	return FontColor.create( 'color', color, 'fontBland', this );
};

/*
| Creates a string.
*/
def.proto.StringBland =
	function( string )
{
	return FontStringBland.FontBlandString( this, string );
};

/*
| Applies a transformation scale.
*/
def.proto.transform =
	function( transform )
{
	return this.create( 'size', this.size * transform.scale );
};

/*
| Scales font "units" to actual sizes.
*/
def.lazy._fontScale =
	function( )
{
	const otFont = this.family._opentype;
	return 1 / otFont.unitsPerEm * this.size;
};

/*
| Options passed to opentype.
| For fonts smaller than 8 hinting is disabled.
*/
def.lazy._options =
	function( )
{
	return this.size >= 8 ? opentypeOptionsHinting : opentypeOptions;
};
