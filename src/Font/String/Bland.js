/*
| A string in a font with a specific size but no color.
*/
def.attributes =
{
	// font to use.
	fontBland: { type: 'Font/Bland' },

	// string to draw
	string: { type: 'string' },
};

import { Self as GlyphBland } from '{Font/Glyph/Bland}';

/*
| Measures the advance width.
*/
def.lazy.advanceWidth =
	function( )
{
	const fontBland = this.fontBland;
	return(
		fontBland.family._opentype.getAdvanceWidth( this.string, fontBland.size )
	);
};

/*
| Creation shortcut.
*/
def.static.FontBlandString =
	function( fontBland, string )
{
	return(
		Self.create(
			'fontBland', fontBland,
			'string', string,
		)
	);
};

/*
| Returns the width of the token.
*/
def.lazy.width =
	function( )
{
	const glyphs = this._glyphs;
	let w = 0;

	for( let a = 0, al = glyphs.length - 1; a < al; a++ )
	{
		const glyph = glyphs[ a ];
		w += glyph.advanceWidth;
		w += glyph.kerningValue( glyphs[ a + 1 ] );
	}

	if( glyphs.length > 0 )
	{
		const glyph = glyphs[ glyphs.length -1 ];
		//w += glyph.boundingBoxInt.width;
		w += glyph.advanceWidth;
	}

	return w;
};

/*
| The glyphs.
*/
def.lazy._glyphs =
	function( )
{
	const fontBland = this.fontBland;

	let otGlyphs = fontBland.family._opentype.stringToGlyphs( this.string, fontBland._options );

	// FUTURE
	//let otGlyphIndexes =
	//    this._opentype( ).stringToGlyphIndexes( this.string, fontBland._options );

	let glyphs = [ ];
	//for( let index of otGlyphIndexes )
	for( let glyph of otGlyphs )
	{
		glyphs.push(
			GlyphBland.FontBlandIndex( fontBland, glyph.index )
		);
	}

	return glyphs;
};
