/*
| A shape or path in a display.
*/
def.attributes =
{
	// color of the border
	color: { type: [ 'undefined', 'Color/Self' ], json: true },

	dashed: { type: [ 'undefined', 'Style/Dashed' ], json: true },

	// the figure to draw
	figure: { type: [ '< Figure/Types' ], json: true },

	// fill color or gradient
	fill:
	{
		type:
		[
			'undefined',
			'Color/Self',
			'Style/Gradient/Askew',
			'Style/Gradient/Radial',
		],

		json: true
	},

	// name of the clint for debugging & testsuite
	name: { type: [ 'undefined', 'string' ], json: true },

	// opt out of grid fitting
	nogrid: { type: [ 'undefined', 'boolean'], json: true },

	// border width
	width: { type: [ 'undefined', 'number' ], json: true },
};

def.json = true;

/*
| Shortcut.
*/
def.static.Figure =
	( figure ) =>
	Self.create( 'figure', figure );

/*
| Shortcut.
*/
def.static.FigureColor =
	( figure, color ) =>
	Self.create( 'color', color, 'figure', figure, 'width', 1 );

/*
| Shortcut.
*/
def.static.FigureColorFill =
	( figure, color, fill ) =>
	Self.create( 'color', color, 'figure', figure, 'fill', fill, 'width', 1 );

/*
| Shortcut.
*/
def.static.FigureColorFillWidth =
	( figure, color, fill, width ) =>
	Self.create( 'color', color, 'figure', figure, 'fill', fill, 'width', width );

/*
| Shortcut.
*/
def.static.FigureColorNogrid =
	( figure, color ) =>
	Self.create( 'color', color, 'figure', figure, 'nogrid', true, 'width', 1 );

/*
| Shortcut.
*/
def.static.FigureColorWidth =
	( figure, color, width ) =>
	Self.create( 'color', color, 'figure', figure, 'width', width );

/*
| Shortcut.
*/
def.static.FigureColorWidthNogrid =
	( figure, color, width ) =>
	Self.create( 'color', color, 'figure', figure, 'nogrid', true, 'width', width );

/*
| Shortcut.
*/
def.static.FigureFill =
	( figure, fill ) =>
	Self.create( 'fill', fill, 'figure', figure );

/*
| Shortcut.
*/
def.static.FigureFillNogrid =
	( figure, fill ) =>
	Self.create( 'fill', fill, 'figure', figure, 'nogrid', true );

/*
| Shortcut.
*/
def.static.FigureFillNogridName =
	( figure, fill, name ) =>
	Self.create( 'fill', fill, 'figure', figure, 'nogrid', true, 'name', name );

/*
| Shortcut.
*/
def.static.FigureFillNogrid =
	( figure, fill ) =>
	Self.create( 'fill', fill, 'figure', figure, 'nogrid', true );

/*
| Shortcut.
*/
def.static.FigureNogrid =
	( figure ) =>
	Self.create( 'figure', figure, 'nogrid', true );

/*
| Extra checking.
*/
def.proto._check =
	function( )
{
/**/if( CHECK )
/**/{
/**/	if( this.width === undefined && this.color !== undefined ) throw new Error( );
/**/	if( this.width !== undefined && this.color === undefined ) throw new Error( );
/**/}
};
