/*
| Puts image data on display.
*/
def.attributes =
{
	// the data to display
	data: { type: 'protean' },

	// name of the clint for debugging & testsuite
	name: { type: [ 'undefined', 'string' ], json: true },

	// position of the data
	pos: { type: 'Figure/Point', json: true }
};

def.json = true;
