/*
| A coordinate transformation that does nothing.
*/
def.singleton = true;

def.extend = 'Transform/Base';

def.json = true;

import { Self as Point             } from '{Figure/Point}';
import { Self as RotateScaleOffset } from '{Transform/RotateScaleOffset}';
import { Self as Scale             } from '{Transform/Scale}';
import { Self as ScaleOffset       } from '{Transform/ScaleOffset}';

/*
| Returns a transform which does the same
| as the combination of this and t.
|
| ~t: transform to combine.
*/
def.proto.combine =
	function( t )
{
	return t;
};

/*
| Returns a transformed distance.
*/
def.proto.d =
	function( d )
{
	return d;
};

/*
| Returns a detransformed distance.
*/
def.proto.ded =
	function( d )
{
	return d;
};

/*
| Returns a reverse transformed x value.
*/
def.proto.dex =
	function( x )
{
/**/if( CHECK && ( typeof( x ) !== 'number' || arguments.length !== 1 ) ) throw new Error( );

	return x;
};

/*
| Returns the reverse transformed y value.
*/
def.proto.dey =
	function( y )
{
/**/if( CHECK && ( typeof( x ) !== 'number' || arguments.length !== 1 ) ) throw new Error( );

	return y;
};

/*
| Changes the offset of this transform.
*/
def.proto.addOffset =
def.proto.setOffset =
def.static.addOffset =
def.static.setOffset =
	function( offset )
{
	// FIXME make an offset only transform
	return ScaleOffset._create( 'offset', offset, 'scale', 1 );
};

/*
| Adds rotation to the transfrom
*/
def.proto.addRotate =
def.static.setRotate =
	function( rotation )
{
	// FUTURE make a rotate only transform
	return(
		RotateScaleOffset._create(
			'offset', Point.zero,
			'rotation', rotation,
			'scale', 1,
		)
	);
};

/*
| Normal has no offset.
*/
def.lazy.offset =
	( ) =>
	Point.zero;

/*
| Normal has a 1 scale.
*/
def.proto.scale = 1;

/*
| Changes the scale of this transform.
*/
def.proto.scaleBy =
def.proto.setScale =
def.static.scaleBy =
def.static.setScale =
	function( scale )
{
	return Scale._create( 'scale', scale );
};

/*
| Returns a transform with the same scale as this, but with a zero offset.
*/
def.lazy.scaleOnly =
	function( )
{
	return this;
};

/*
| Changes the offset and scale of this transform.
*/
def.proto.setScaleOffset =
def.static.setScaleOffset =
	function( scale, offset )
{
	return ScaleOffset._create( 'offset', offset, 'scale', scale );
};

/*
| Returns a transformed x value.
*/
def.proto.x =
	function( x )
{
/**/if( CHECK && ( typeof( x ) !== 'number' || arguments.length !== 1 ) ) throw new Error( );

	return x;
};

/*
| Returns a transformed y value.
*/
def.proto.y =
	function( y )
{
/**/if( CHECK && ( typeof( y ) !== 'number' || arguments.length !== 1 ) ) throw new Error( );

	return y;
};
