/*
| A radial gradient.
*/
def.extend = 'list@Style/Gradient/ColorStop';
def.json = true;
