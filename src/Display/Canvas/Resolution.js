/*
| The resolution of a canvas determines details about fixing to
| device pixels to create sharp rendering.
*/
def.attributes =
{
	// device pixel ratio
	devicePixelRatio: { type: 'number', json: true },

	// amount of over buffering
	superSampling: { type: 'number', json: true },
};

def.json = true;

/*
| Creates a resolution for the current device.
|
| ~superSampling: if > 1 draw at higher resolution
*/
def.static.CurrentDevice =
	function( superSampling )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	const dpr = window.devicePixelRatio;
	if( !dpr ) throw new Error( );

	return(
		Self.create(
			'devicePixelRatio', dpr,
			'superSampling', superSampling
		)
	);
};

/*
| Returns dpi of current resolution.
*/
def.lazy.dpi =
	function( )
{
	return 96 * this.ratio;
};

/*
| Fits a value to the pixel grid.
*/
def.proto.fit =
	function( v )
{
	const sus = this.superSampling;
	return Math.round( v / sus ) * sus;
};

/*
| Combination of devicePixelRatio and superSamplinging
*/
def.lazy.ratio =
	function( )
{
	return this.devicePixelRatio * this.superSampling;
};

/*
| The one to one resolution.
*/
def.staticLazy.Identity =
	( ) =>
	Self.create(
		'devicePixelRatio', 1,
		'superSampling', 1,
	);
