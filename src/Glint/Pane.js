/*
| A rectangular cut-out.
|
| May do caching.
*/
def.attributes =
{
	// background color, undefined is transparent
	background: { type: [ 'undefined', 'Color/Self' ], json: true },

	// the glints to draw in the pane
	glint: { type: [ '< Glint/Types' ], json: true },

	// name of the clint for debugging & testsuite
	name: { type: [ 'undefined', 'string' ], json: true },

	// canvas resolution
	resolution: { type: 'Display/Canvas/Resolution', json: true },

	// the size of the pane
	size: { type: 'Figure/Size', json: true },
};

def.json = true;

import { Self as Display } from '{Display/Canvas/Self}';

/*
| Creates a subcanvas for rendering and caching.
*/
def.lazy._canvasDisplay =
	function( )
{
	const resolution = this.resolution;

	const display =
		Display.NewCanvas(
			this.size.scaleCeil( 1 / resolution.ratio ),
			resolution,
			this.glint,
			this.background,
		);

	display.render( );

	return display;
};
