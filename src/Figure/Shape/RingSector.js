/*
| A ring sector (or annulus sector).
|
|
|     A       ⣿⣿⣿⣿⣿⣿⣿⣧⣶⣤⣤⢀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
|     |       ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣦⣄⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
|     |       ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣤⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
|     |       ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣄⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
|     |       ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣆⡀⠀⠀⠀⠀⠀⠀⠀⠀
|     |   A   .⠀⠈⠉⠉⠛⠛⠿⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣆⠀⠀⠀⠀⠀⠀⠀
|     |   |   .⠀⠀⠀⠀⠀⠀⠀⠀⠈⠙⠻⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⡀⠀⠀⠀⠀⠀
|  ro |   |   .⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⠀
|     |   |   .⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⢿⣿⣿⣿⣿⣿⣿⣿⣿⡟'
|     |   |   .⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⢿⣿⣿⣿⣿⡟'
|     | ri|   .⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⢻⡟'⠀
|     |   |   .⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀.⠀`⠀⠀⠀⠀
|     |   |   .          .  `
|     |   |   .     .  `
|     v   v   .  `
*/
def.attributes =
{
	// starting angle
	angle0: { type: '<Figure/Angle/Types', json: true },

	// stopping angle
	angle1: { type: '<Figure/Angle/Types', json: true },

	// center position of circle
	pCircle: { type: 'Figure/Point', json: true },

	// inner radius
	ri: { type: 'number', json: true },

	// outer radius
	ro: { type: 'number', json: true },
};

def.json = true;

import { Self as Angle         } from '{Figure/Angle/Self}';
import { Self as Display       } from '{Display/Canvas/Self}';
import { Self as Path          } from '{Figure/Path/Self}';
import { Self as TransformBase } from '{Transform/Base}';

/*
| Returns a pin moved by x/y.
*/
def.proto.add =
	function( x, y )
{
	return this.create( 'pCircle', this.pCircle.add( x, y ) );
};

/*
| Expands( or shrinks ) the pin by d.
*/
def.lazyFunc.envelope =
	function( d )
{
	throw new Error( );
};

/*
| For now no gradients.
*/
def.proto.gradientPC = undefined;
def.proto.gradientR0 = undefined;
def.proto.gradientR1 = undefined;

/*
| The path of the pin.
*/
def.lazy.path =
	function( )
{
	const angle0 = this.angle0;
	const angle1 = this.angle1;
	const pc     = this.pCircle;
	const ri     = this.ri;
	const ro     = this.ro;

	const pi0 = pc.add( ri * angle0.cos, -ri * angle0.sin );
	const pi1 = pc.add( ri * angle1.cos, -ri * angle1.sin );

	const po0 = pc.add( ro * angle0.cos, -ro * angle0.sin );
	const po1 = pc.add( ro * angle1.cos, -ro * angle1.sin );

	const phi = angle1.radiansBetween( angle0 );
	const kro = 4 / 3 * ro * Math.tan( phi / 4 );
	const poc0 = po0.add( -kro * angle0.sin, -kro * angle0.cos );
	const poc1 = po1.add(  kro * angle1.sin,  kro * angle1.cos );

	const kri = 4 / 3 * ri * Math.tan( phi / 4 );
	const pic0 = pi0.add( -kri * angle0.sin, -kri * angle0.cos );
	const pic1 = pi1.add(  kri * angle1.sin,  kri * angle1.cos );

	return(
		Path.Plan(
			'pc',     pc,
			'start',  pi0,
			'line',   po0,
			'bezier', poc0, poc1, po1,
			'line',   pi1,
			'bezier', pic1, pic0, 'close',
		)
	);
};

/*
| point in the center of the sector.
*/
def.lazy.pc =
def.lazy.pCenterSector =
	function( )
{
	const angle0 = this.angle0;
	const angle1 = this.angle1;
	const angle05 = Angle.Radians( angle0.radians - angle0.radiansBetween( angle1 ) / 2 );

	const r05    = ( this.ri + this.ro ) / 2;
	const pc     = this.pCircle;
	return pc.add( r05 * angle05.cos, -r05 * angle05.sin );
};

/*
| Returns a pin moved by x/y.
*/
def.proto.sub =
	function( x, y )
{
	return this.create( 'pos', this.pos.sub( x, y ) );
};

/*
| Returns a transformed pin.
*/
def.proto.transform =
	function( transform )
{
/**/if( CHECK && !TransformBase.isa( transform ) ) throw new Error( );

	return(
		this.create(
			'pos', this.pos.transform( transform ),
			'width', transform.d( this.width ),
			'height', transform.d( this.height ),
		)
	);
};

/*
| Returns true if p is within the path.
*/
def.proto.within =
	function( p )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	return Display.helper.within( p, this );
};
