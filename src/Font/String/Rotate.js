/*
| A string in a font with a specific size and color.
| Rotated.
*/
def.attributes =
{
	// face of the font
	fontColor: { type: 'Font/Color' },

	// the bland string to draw
	stringBland: { type: 'Font/String/Bland' },

	// the rotation angle
	rotation: { type: [ '< Figure/Angle/Types' ] },
};

/*
| Glyphs for fonts larger than this won't be cached.
*/
const glyphCacheLimit = 250;

import { Self as FontStringBland } from '{Font/String/Bland}';
import { Self as GlyphColor      } from '{Font/Glyph/Color}';
import { Self as Point           } from '{Figure/Point}';
import { Self as TransformNormal } from '{Transform/Normal}';

/*
| Measures the advance width.
*/
def.lazy.advanceWidth =
	function( )
{
	return this.stringBland.advanceWidth;
};

/*
| Create Shortcut.
|
| ~fontColor: the colored font to use.
| ~string: the string
*/
def.static.FontColorRotationString =
	function( fontColor, rotation, string )
{
	return(
		Self.create(
			'fontColor', fontColor,
			'rotation', rotation,
			'stringBland', FontStringBland.FontBlandString( fontColor.fontBland, string ),
		)
	);
};

/*
| Returns the width of the token.
*/
def.lazy.width =
	function( )
{
	return this.stringBland.width;
};

/*
| Draws the string.
|
| ~point:      point
| ~align:      horizontal align
| ~base:       vertial align
| ~resolution: resolution
| ~cx:         canvas context to draw it on.
*/
def.proto._drawOnCanvas =
	function( point, align, base, resolution, cx )
{
/**/if( CHECK && arguments.length !== 5 ) throw new Error( );

	const fontColor = this.fontColor;
	const fontBland = fontColor.fontBland;
	const size = fontBland.size;

	const fontScale = fontBland._fontScale;
	const glyphs = this._glyphs;

	const x0 = point.x;
	const y0 = point.y;
	let x = 0;
	let y = 0;

	const rotation = this.rotation;
	const t2 = TransformNormal.setRotate( rotation );

	switch( align )
	{
		case 'center':
			x -= this.width / 2;
			break;

		case 'left':
			break;

		case 'right':
			x -= this.width;
			break;

		default: throw new Error( );
	}

	switch( base )
	{
		case 'alphabetic':
			break;

		case 'middle':
			y += fontBland.family.capHeight * fontScale / 2 - 0.5;
			break;

		default: throw new Error( );
	}

	let glyph;
	for( let a = 0, alen = glyphs.length; a < alen; a++ )
	{
		glyph = glyphs[ a ];

		if( size >= glyphCacheLimit )
		{
			const cos = rotation.cos;
			const sin = rotation.sin;
			cx.setTransform(
				cos, -sin, sin, cos,
				resolution.fit( t2.x( x, y ) + x0 ),
				resolution.fit( t2.y( x, y ) + y0 )
			);
			glyph._drawOnCanvas( cx, Point.zero );
		}
		else
		{
			const gCanvas = glyph._canvas;
			const gCPos = glyph._canvasPos;
			if( gCanvas !== true )
			{
				const cos = rotation.cos;
				const sin = rotation.sin;
				const xx = x + gCPos.x;
				const yy = y + gCPos.y;
				cx.setTransform(
					cos, -sin, sin, cos,
					resolution.fit( t2.x( xx, yy ) + x0 ),
					resolution.fit( t2.y( xx, yy ) + y0 )
				);
				cx.drawImage( gCanvas, 0, 0 );
			}
		}

		if( a + 1 < alen )
		{
			const aw = glyph.advanceWidth;
			if( aw ) x += aw;
			x += glyph.kerningValue( glyphs[ a + 1 ] ) * fontScale;
		}
	}

	cx.setTransform( 1, 0, 0, 1, 0, 0 );
};

/*
| The glyphs.
*/
def.lazy._glyphs =
	function( )
{
	const fontColor = this.fontColor;
	const color = fontColor.color;

	let bGlyphs = this.stringBland._glyphs;
	let glyphs = [ ];

	for( let bGlyph of bGlyphs )
	{
		glyphs.push(
			GlyphColor.create( 'color', color, 'glyphBland', bGlyph )
		);
	}

	return glyphs;
};
