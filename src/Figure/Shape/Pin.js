/*
| A pin.
|
|         .-'""'-.           A
|       .'        '.         | height
|      /            \        |
|     ;              ;       |
|     ;              ;       |
|     ''            ''       |
|       \          /         |
|     '  \        /  '       |
|         \      /           |
|     '    \    /    '       |
|           \  /             |
|     '      \/      + - - - V
|     |              |
|     <--------------> width

*/
def.attributes =
{
	// height
	height: { type: 'number', json: true },

	// position
	pos: { type: 'Figure/Point', json: true },

	// width
	width: { type: 'number', json: true },
};

def.json = true;

import { Self as Display       } from '{Display/Canvas/Self}';
import { Self as Path          } from '{Figure/Path/Self}';
import { Self as Point         } from '{Figure/Point}';
import { Self as TransformBase } from '{Transform/Base}';

/*
| Returns a pin moved by x/y.
*/
def.proto.add =
	function( a0, a1 )
{
	return this.create( 'pos', this.pos.add( a0, a1 ) );
};

/*
| Expands( or shrinks ) the pin by d.
*/
def.lazyFunc.envelope =
	function( d )
{
/**/if( CHECK && typeof( d ) !== 'number' ) throw new Error( );

	return(
		this.create(
			'pos', this.pos.add( -d, -d ),
			'height', this.height + 2 * d,
			'width', this.width + 2 * d
		)
	);
};

/*
| For now no gradients for pins.
*/
def.proto.gradientPC = undefined;
def.proto.gradientR0 = undefined;
def.proto.gradientR1 = undefined;

/*
| Gets the intersections with something else.
|
| ~figure: figure to intersect with
*/
def.proto.intersectsPoint =
	function( figure )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	return this.path.intersectsPoint( figure );
};

/*
| Point in the center.
*/
def.lazy.pc =
	function( )
{
	const w05 = this.width / 2;
	return this.pos.add( w05, w05 );
};

/*
| The path of the pin.
*/
def.lazy.path =
	function( )
{
	const h = this.height;
	const p = this.pos;
	const w = this.width;
	const w05 = w / 2;

	const cx = p.x + w05;
	const cy = p.y + w05;

	const psx = cx;
	const psy = p.y + h;

	let ptan1, ptan2;

	// finds the tangent points
	{
		const dx = cx - psx;
		const dy = cy - psy;
		const dd = Math.sqrt( dx * dx + dy * dy );

		const a = Math.asin( w05 / dd );
		const b = Math.atan2( dy, dx );

		const t1 = b - a;
		const t2 = b + a;

		// barrier against NaNs
		if( t1 !== t1 || t2 !== t2 )
		{
			// just make it a circle in these cases
			const h05 = h / 2;
			return(
				Path.Plan(
					'pc', this.pc,
					'start', p.add( -w05, 0 ),
					'qbend', p.add( 0, -h05 ),
					'qbend', p.add( w05, 0 ),
					'qbend', p.add( 0, h05 ),
					'qbend', 'close',
				)
			);
		}

		ptan1 = Point.XY( cx + w05 * Math.sin( t1 ), cy - w05 * Math.cos( t1 ) );
		ptan2 = Point.XY( cx - w05 * Math.sin( t2 ), cy + w05 * Math.cos( t2 ) );
	}

	//const ps = pos.add( h, w / 2;

	const ptq1 = p.add( 0, w05 ); // = p1 of bezier1
	let ptac11, ptac12; // control points of bezier1

	// calculates the control points to create arcs from tangent to circle quarter.
	{
		const ax = ptan1.x - cx;
		const ay = ptan1.y - cy;
		const bx = ptq1.x - cx;
		const by = ptq1.y - cy;

		const q1 = ax * ax + ay * ay;
		const q2 = q1 + ax * bx + ay * by;
		const k2 = ( 4 / 3 ) * ( Math.sqrt(2 * q1 * q2) - q2 ) / ( ax * by - ay * bx );

		ptac11 = Point.XY( cx + ax - k2 * ay, cy + ay + k2 * ax );
		ptac12 = Point.XY( cx + bx + k2 * by, cy + by - k2 * bx );
	}

	// and for the other side
	let ptac21, ptac22; // control points of bezier2
	const ptq2 = p.add( w, w05 ); // = p1 of bezier2
	{
		const ax = ptq2.x - cx;
		const ay = ptq2.y - cy;
		const bx = ptan2.x - cx;
		const by = ptan2.y - cy;

		const q1 = ax * ax + ay * ay;
		const q2 = q1 + ax * bx + ay * by;
		const k2 = ( 4 / 3 ) * ( Math.sqrt(2 * q1 * q2) - q2 ) / ( ax * by - ay * bx );

		ptac21 = Point.XY( cx + ax - k2 * ay, cy + ay + k2 * ax );
		ptac22 = Point.XY( cx + bx + k2 * by, cy + by - k2 * bx );
	}

	return(
		Path.Plan(
			'pc', this.pc,
			'start', Point.XY( psx, psy ),
			'line', ptan1,
			'bezier', ptac11, ptac12, ptq1,
			'qbend', p.add( w05, 0 ),
			'qbend', ptq2,
			'bezier', ptac21, ptac22, ptan2,
			'line', 'close'
		)
	);
};

/*
| Returns a pin moved by x/y.
*/
def.proto.sub =
	function( x, y )
{
	return this.create( 'pos', this.pos.sub( x, y ) );
};

/*
| Returns a transformed pin.
*/
def.proto.transform =
	function( transform )
{
/**/if( CHECK && !TransformBase.isa( transform ) ) throw new Error( );

	return(
		this.create(
			'pos', this.pos.transform( transform ),
			'width', transform.d( this.width ),
			'height', transform.d( this.height ),
		)
	);
};

/*
| Returns true if p is within the path.
*/
def.proto.within =
	function( p )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	return Display.helper.within( p, this );
};
