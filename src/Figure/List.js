/*
| A list of shapes or paths.
*/
def.extend = 'list@(<Figure/Shape/Types,Figure/Path/Self,<Figure/Path/Types)';
def.json = true;

import { Self as Point         } from '{Figure/Point}';
import { Self as Rect          } from '{Figure/Shape/Rect}';
import { Self as TransformBase } from '{Transform/Base}';

/*
| Returns a moved FigureList.
*/
def.proto.add =
	function( /* p or x,y */ )
{
	const list = [ ];
	for( let figure of this )
	{
		list.push( figure.add.apply( figure, arguments ) );
	}

	return this.create( 'list:init', list );
};

/*
| Returns a FigureList enveloping this figure by
| +/- distance.
|
| ~d: distance
*/
def.lazyFunc.envelope =
	function( d )
{
	const list = [ ];
	for( let figure of this )
	{
		list.push( figure.envelope( d ) );
	}

	return this.create( 'list:init', list );
};

/*
| Returns a transformed FigureList.
*/
def.proto.transform =
	function( transform )
{
/**/if( CHECK && !TransformBase.isa( transform ) ) throw new Error( );

	const list = [ ];
	for( let figure of this )
	{
		list.push( figure.transform( transform ) );
	}
	return this.create( 'list:init', list );
};

/*
| Returns true if point is within the FigureList.
*/
def.proto.within =
	function( p )
{
	for( let figure of this )
	{
		if( figure.within( p ) )
		{
			return true;
		}
	}

	return false;
};

/*
| The zone containing the zones all list items.
*/
def.lazy.zone =
	function( )
{
	if( this.length === 0 ) throw new Error( );

	let x0, y0, x1, y1;
	{
		const f0 = this.get( 0 );
		const sz = f0.zone;
		const p0 = sz.pos;
		const p1 = sz.pse;
		x0 = p0.x;
		y0 = p0.y;
		x1 = p1.x;
		y1 = p1.y;
	}

	for( let a = 1, alen = this.length; a < alen; a++ )
	{
		const f = this.get( a );
		const sz = f.zone;
		const p0 = sz.pos;
		const p1 = sz.pse;
		const sx0 = p0.x;
		const sy0 = p0.y;
		const sx1 = p1.x;
		const sy1 = p1.y;

		if( sx0 < x0 ) x0 = sx0;
		if( sy0 < y0 ) y0 = sy0;
		if( sx1 > x1 ) x1 = sx1;
		if( sy1 > y1 ) y1 = sy1;
	}

	return(
		Rect.PosPse(
			Point.XY( x0, y0 ),
			Point.XY( x1, y1 )
		)
	);
};
