/*
| A rectangle.
|
| Theoretically it would be possible to store the "north west" point and the
| "south east" point instead of "pos" in north-west and width and height.
|
| However, then due to floating point discrepancies moving the rectangle
| would ever so slightly change it size and caching fails.
*/
def.attributes =
{
	// position of the rect
	pos: { type: 'Figure/Point', json: true },

	// height of the rect
	height: { type: 'number', json: true },

	// width of the rect
	width: { type: 'number', json: true }
};

def.json = true;

import { Self as Margin               } from '{Figure/Margin}';
import { Self as Point                } from '{Figure/Point}';
import { Self as Segment              } from '{Figure/Path/Segment}';
import { Self as Size                 } from '{Figure/Size}';
import { Self as TransformBase        } from '{Transform/Base}';

/*
| Returns a rect moved by a point or x/y.
|
| add( point )   -or-
| add( x, y  )
*/
def.proto.add =
	function( /* ... */ )
{
	const pos = this.pos;
	return this.create( 'pos', pos.add.apply( pos, arguments ) );
};

/*
| Creates a rect by two arbitrary corner points.
|
| ~p0: one point
| ~p1: another point
*/
def.static.Arbitrary =
	function( p0, p1 )
{
	if( p1.x >= p0.x && p1.y >= p0.y )
	{
		return Self.PosWidthHeight( p0, p1.x - p0.x, p1.y - p0.y );
	}
	else if( p0.x >= p1.x && p0.y >= p1.y )
	{
		return Self.PosWidthHeight( p1, p0.x - p1.x, p0.y - p1.y );
	}
	else if( p1.x >= p0.x && p0.y >= p1.y )
	{
		return Self.PosWidthHeight( Point.XY( p0.x, p1.y ), p1.x - p0.x, p0.y - p1.y );
	}
	else if( p0.x >= p1.x && p1.y >= p0.y )
	{
		return Self.PosWidthHeight( Point.XY( p1.x, p0.y ), p0.x - p1.x, p1.y - p0.y );
	}

	// this should never happen
	throw new Error( );
};

/*
| Returns this point scaled by
| scaleX, scaleY relative to the base point.
*/
def.proto.baseScaleAction =
	function( action, ax, ay )
{
	return this.baseScaleXY( action.scaleX, action.scaleY, action.pBase, ax, ay );
};

/*
| Returns this rect scaled by
| scaleX, scaleY relative to the base point.
|
| ~scaleX: x scaling
| ~scaleY: y scaling
| ~pBase: base point
| ~ax: x value to be added
| ~ay: y value to be added
*/
def.proto.baseScaleXY =
	function( scaleX, scaleY, pBase, ax, ay )
{
	if( scaleX === 1 && scaleY === 1 )
	{
		return this.add( ax, ay );
	}

	const pos = this.pos.baseScaleXY( scaleX, scaleY, pBase, ax, ay );
	const pse = this.pse.baseScaleXY( scaleX, scaleY, pBase, ax, ay );

	return Self.PosWidthHeight( pos, pse.x - pos.x, pse.y - pos.y );
};

/*
| Returns this detransformed rect.
*/
def.proto.detransform =
	function( transform )
{
/**/if( CHECK && !TransformBase.isa( transform ) ) throw new Error( );

	return(
		this.create(
			'pos',    this.pos.detransform( transform ),
			'width',  transform.ded( this.width ),
			'height', transform.ded( this.height )
		)
	);
};

/*
| Returns a rect which is at least as large a size.
|
| ~min: minimum size
*/
def.proto.ensureMinSize =
	function( min )
{
/**/if( CHECK && min.ti2ctype !== Size ) throw new Error( );

	const h = this.height;
	const w = this.width;
	const mh = min.height;
	const mw = min.width;

	return(
		this.create(
			'height', Math.max( h, mh ),
			'width',  Math.max( w, mw ),
		)
	);
};

/*
| Expands( or shrinks ) the rect by d.
|
| ~d: distance to envelope
*/
def.lazyFunc.envelope =
	function( d )
{
/**/if( CHECK && typeof( d ) !== 'number' ) throw new Error( );

	return(
		Self.PosWidthHeight(
			this.pos.add( -d, -d ),
			this.width + 2 * d, this.height + 2 * d,
		)
	);
};

/*
| Gets the intersections with something else.
|
| ~figure: figure to intersect with
*/
def.proto.intersectsPoint =
	function( figure )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	return(
		this.segmentN.intersectsPoint( figure )
		|| this.segmentE.intersectsPoint( figure )
		|| this.segmentS.intersectsPoint( figure )
		|| this.segmentW.intersectsPoint( figure )
	);
};

/*
| Returns true if this rect overlaps with another.
*/
def.proto.overlaps =
	function( rect )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( rect.ti2ctype !== Self ) throw new Error( );
/**/}

	const tpos = this.pos;
	const tpse = this.pse;
	const rpos = rect.pos;
	const rpse = rect.pse;

	return(
		rpos.x < tpse.x
		&& tpos.x < rpse.x
		&& tpos.y < rpse.y
		&& rpos.y < tpse.y
	);
};

/*
| Point in the center.
*/
def.lazy.pc =
	function( )
{
	return this.pos.add( this.width / 2, this.height / 2 );
};

/*
| East point.
*/
def.lazy.pe =
	function( )
{
	return this.pos.add( this.width, this.height / 2 );
};

/*
| North point.
*/
def.lazy.pn =
	function( )
{
	return this.pos.add( this.width / 2, 0 );
};

/*
| North east point.
*/
def.lazy.pne =
	function( )
{
	return this.pos.add( this.width, 0 );
};

// Note: 'pnw' is 'pos'.

/*
| Shortcut to create a rect by specifying position and size.
*/
def.static.PosSize =
	( pos, size ) =>
	Self.create(
		'pos', pos,
		'width', size.width,
		'height', size.height
	);

/*
| Creates a rectangle by its NW and SE positions
*/
def.static.PosPse =
	( pos, pse ) =>
{
	return(
		Self.create(
			'pos', pos,
			'width', pse.x - pos.x,
			'height', pse.y - pos.y
		)
	);
};

/*
| Shortcut to create a rect by specifying position and size.
*/
def.static.PosWidthHeight =
	( pos, width, height ) =>
	Self.create(
		'pos', pos,
		'width', width,
		'height', height
	);

/*
| South point.
*/
def.lazy.ps =
	function( )
{
	return this.pos.add( this.width / 2, this.height );
};

/*
| South east point.
*/
def.lazy.pse =
	function( )
{
	return this.pos.add( this.width, this.height );
};

/*
| South west point.
*/
def.lazy.psw =
	function( )
{
	return this.pos.add( 0, this.height );
};

/*
| West point.
*/
def.lazy.pw =
	function( )
{
	return this.pos.add( 0, this.height / 2 );
};

/*
| Returns a rectangle thats reduced on every side by a margin object
*/
def.proto.reduce =
	function( margin )
{
/**/if( CHECK && margin.ti2ctype !== Margin ) throw new Error( );

	// allows margins to reduce the rect to zero size without erroring.
	return(
		Self.PosWidthHeight(
			this.pos.add( margin.e, margin.n ),
			this.width - margin.e - margin.w,
			this.height - margin.n - margin.s
		)
	);
};

/*
| East line segment.
*/
def.lazy.segmentE =
	function( )
{
	return Segment.P0P1( this.pne, this.pse );
};

/*
| North line segment.
*/
def.lazy.segmentN =
	function( )
{
	return Segment.P0P1( this.pos, this.pne );
};

/*
| South line segment.
*/
def.lazy.segmentS =
	function( )
{
	return Segment.P0P1( this.pse, this.psw );
};

/*
| West line segment.
*/
def.lazy.segmentW =
	function( )
{
	return Segment.P0P1( this.psw, this.pos );
};

/*
| Shortcut to recreate the rect at
| current pos with a given size.
*/
def.proto.Size =
	function( size )
{
	return this.create( 'height', size.height, 'width', size.width );
};

/*
| A size tim matching this rect.
*/
def.lazy.size =
	function( )
{
	return Size.WH( this.width, this.height );
};

/*
| Returns a rect substracted by a point or x/y.
*/
def.proto.sub =
	function( ...args )
{
	const pos = this.pos;

	return this.create( 'pos', pos.sub.apply( pos, args ) );
};

/*
| Returns this transformed rect.
*/
def.proto.transform =
	function( transform )
{
/**/if( CHECK && !TransformBase.isa( transform ) ) throw new Error( );

	return(
		Self.PosWidthHeight(
			this.pos.transform( transform ),
			transform.d( this.width ), transform.d( this.height )
		)
	);
};

/*
| Returns true if the figure is within this rect.
|
| ~fig: figure to test
*/
def.proto.within =
	function( fig )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	switch( fig.ti2ctype )
	{
		case Point:
		{
			const x = fig.x;
			const y = fig.y;
			const pos = this.pos;
			return(
				x >= pos.x
				&& y >= pos.y
				&& x <= pos.x + this.width
				&& y <= pos.y + this.height
			);
		}

		case Self:
		{
			return(
				this.within( fig.pos )
				&& this.within( fig.pse )
			);
		}

		default: throw new Error( );
	}
};

/*
| A zero rect.
*/
def.staticLazy.zero =
	( ) =>
	Self.PosWidthHeight( Point.zero, 0,  0 );

/*
| A rectangle of same size with pos at 0/0
*/
def.lazy.zeroPos =
	function( )
{
	return this.create( 'pos', Point.zero );
};
