/*
| A cubic bezier curve.
*/
def.attributes =
{
	// start point
	p0: { type: 'Figure/Point', json: true },

	// end point
	p1: { type: 'Figure/Point', json: true },

	// control point 0
	pco0: { type: 'Figure/Point', json: true },

	// control point 1
	pco1: { type: 'Figure/Point', json: true },
};

def.json = true;

import { Self as Angle      } from '{Figure/Angle/Self}';
import { Self as ConvexHull } from '{EMath/ConvexHull}';
import { Self as EMath      } from '{EMath/Self}';
import { Self as Line       } from '{Figure/Path/Line}';
import { Self as Parts      } from '{Figure/Path/Parts}';
import { Self as Path       } from '{Figure/Path/Self}';
import { Self as Point      } from '{Figure/Point}';
import { Self as Rect       } from '{Figure/Shape/Rect}';
import { Self as Segment    } from '{Figure/Path/Segment}';

// Stops flatline clipping at this epsilon neighbourhood
const flEpsilon = 1e-5;

// Minimum flatness for enveloping
const envelopeMinFlatness = 1.01;

// Considers a bezier to be a segment below or at this value.
// Turned off makes issues with intersections for envelopes.
const segFlatness = 1.0001;

// If p0-p1 distance lower than this, assume a line
const flatnessEpsilon = 1e-3;

// Ignore "intersections" on neighbours near this 0/1 values.
const neighbourEpsilon = 0.05;

// Epsilon for projections
const projectEpsilon = 0.0001;

const abs = Math.abs;
const atan2 = Math.atan2;
const cos = Math.cos;
const max = Math.max;
const min = Math.min;
const pi = Math.PI;
const pi05 = pi / 2;
const sin = Math.sin;
const sqrt = Math.sqrt;

/*
| Angle at p0.
*/
def.lazy.angle0 =
	function( )
{
	return Angle.P0P1( this.pco0, this.p0 );
};

/*
| Angle at p1.
*/
def.lazy.angle1 =
	function( )
{
	return Angle.P0P1( this.pco1, this.p1 );
};

/*
| Moves the bezier by p or x/y.
|
| ~args: p or x/y.
*/
def.proto.add =
	function( ...args )
{
	const p0 = this.p0;
	const pco0 = this.pco0;
	const pco1 = this.pco1;
	const p1 = this.p1;

	return(
		this.create(
			'p0', p0.add.apply( p0, args ),
			'pco0', pco0.add.apply( pco0, args ),
			'pco1', pco1.add.apply( pco1, args ),
			'p1', p1.add.apply( p1, args )
		)
	);
};

/*
| Returns the line from p0 to p1.
*/
def.lazy.baseLine =
	function( )
{
	return Line.P0P1( this.p0, this.p1 );
};

/*
| Chips off a part of the bezier.
|
| ~t0: from this at (0..1)
| ~t1: to this at (0..1)
*/
def.proto.chip =
	function( t0, t1 )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( t0 > t1 ) throw new Error( );
/**/}

	if( t0 === 0 && t1 === 1 ) return this;

	if( t0 === t1 ) return undefined;

	let b = this;
	if( t0 > 0 )
	{
		b = b.chipUpper( t0 );
		t1 = ( t1 - t0 ) / ( 1 - t0 );
	}

	if( t1 < 1 )
	{
		b = b.chipLower( t1 );
	}

	return b;
};

/*
| Chips off the lower part from 0 to t.
|
| ~t: split at this point
*/
def.proto.chipLower =
	function( t )
{
/**/if( CHECK )
/**/{
/**/	if( typeof( t ) !== 'number' ) throw new Error( );
/**/	if( !( t > 0 ) ) throw new Error( );
/**/}

	if( t === 1 ) return this;

	const p0 = this.p0;
	const pc0 = this.pco0;
	const pc1 = this.pco1;
	const p1 = this.p1;

	const pc0x = pc0.x;
	const pc0y = pc0.y;
	const pc1x = pc1.x;
	const pc1y = pc1.y;

	const m = 1 - t;

	const qc0x = m*(p0.x) + t*pc0x;
	const qc0y = m*(p0.y) + t*pc0y;
	const h0x = m*pc0x + t*pc1x;
	const h0y = m*pc0y + t*pc1y;
	const rc1x = m*pc1x + t*(p1.x);
	const rc1y = m*pc1y + t*(p1.y);
	const qc1x = m*qc0x + t*h0x;
	const qc1y = m*qc0y + t*h0y;
	const rc0x = m*h0x + t*rc1x;
	const rc0y = m*h0y + t*rc1y;
	const qrx = m*qc1x + t*rc0x;
	const qry = m*qc1y + t*rc0y;

	const qc0 = Point.XY( qc0x, qc0y );
	const qc1 = Point.XY( qc1x, qc1y );
	const qr  = Point.XY( qrx, qry );

	return Self.Points( p0, qc0, qc1, qr );
};

/*
| Chips off the upper part from t to 1.
|
| ~t: split at this
*/
def.proto.chipUpper =
	function( t )
{
/**/if( CHECK )
/**/{
/**/	if( typeof( t ) !== 'number' ) throw new Error( );
/**/	if( !( t < 1 ) ) throw new Error( );
/**/}

	if( t === 0 ) return this;

	const p0 = this.p0;
	const pc0 = this.pco0;
	const pc1 = this.pco1;
	const p1 = this.p1;

	const pc0x = pc0.x;
	const pc0y = pc0.y;
	const pc1x = pc1.x;
	const pc1y = pc1.y;
	const m = 1 - t;

	const qc0x = m*(p0.x) + t*pc0x;
	const qc0y = m*(p0.y) + t*pc0y;
	const h0x = m*pc0x + t*pc1x;
	const h0y = m*pc0y + t*pc1y;
	const rc1x = m*pc1x + t*(p1.x);
	const rc1y = m*pc1y + t*(p1.y);
	const qc1x = m*qc0x + t*h0x;
	const qc1y = m*qc0y + t*h0y;
	const rc0x = m*h0x + t*rc1x;
	const rc0y = m*h0y + t*rc1y;
	const qrx = m*qc1x + t*rc0x;
	const qry = m*qc1y + t*rc0y;

	const qr  = Point.XY( qrx, qry );
	const rc0 = Point.XY( rc0x, rc0y );
	const rc1 = Point.XY( rc1x, rc1y );

	return Self.Points( qr, rc0, rc1, p1 );
};

/*
| Returns something with distance d next to this bezier.
*/
def.lazyFunc.envelope =
	function( d )
{
	const fl = this.flatness;
	if( fl <= envelopeMinFlatness ) return this._approxEnvelope( d, fl <= segFlatness );
	const list = [ ];
	const ap = this._apportion;

	// FIXME simplify
	if( ap.ti2ctype === Path )
	{
		for( let s of this._apportion.parts ) list.push( s.envelope( d ) );
	}
	else
	{
		list.push( ap.envelope( d ) );
	}

	return Path.EpsilonTolerant( list, false );
};

/*
| Returns the flatness factor of the bezier.
*/
def.lazy.flatness =
	function( )
{
	const p0 = this.p0;
	const pco0 = this.pco0;
	const pco1 = this.pco1;
	const p1 = this.p1;

	const l01 = p0.distanceOfPoint( p1 );
	if( l01 < flatnessEpsilon ) return 1;

	return(
		( p0.distanceOfPoint( pco0 )
		+ pco0.distanceOfPoint( pco1 )
		+ pco1.distanceOfPoint( p1 )
		) / l01
	);
};

/*
| Returns the inflections of the bezier.
*/
def.lazy.inflections =
	function( )
{
	if( this.type !== 'serpentine' ) return false;

	return this._roots;
};

/*
| Finds the intersections of the bezier with something else.
|
| Returns an array with doublet at values.
|
| ~figure:      the figure to find intersections with
| ~extended:    if defined figures are to be extended beyond their end.
| ~flip:        if defined flips results
| ~resultArray: if defined appends intersections to this array
| ~isLeft:      true if figure is left of this
| ~isRight:     true if figure is right of this
*/
def.proto.intersectsAt =
	function( figure, extended, flip, resultArray, isLeft, isRight )
{
	const zone = this.zone;
	const fzone = figure.zone;

	if( !zone.overlaps( fzone ) ) return resultArray;

	switch( figure.ti2ctype )
	{
		case Path:
			return figure.intersectsAt( this, extended, !flip, resultArray );

		case Segment:
			return(
				this._intersectsSegmentAt(
					figure, extended, flip, resultArray, isLeft, isRight
				)
			);

		case Self:
			if( this === figure )
			{
				if( this.type !== 'loop' ) return resultArray;

				if( !resultArray ) resultArray = [ ];

				if( !flip )
				{
					resultArray.push( this._roots[ 0 ], this._roots[ 1 ] );
				}
				else
				{
					resultArray.push( this._roots[ 1 ], this._roots[ 0 ] );
				}

				return resultArray;
			}
			if( !resultArray ) resultArray = [ ];
			this._fatLineClipping( figure, 0, 0, 0, 1, 0, 1, flip, resultArray, isLeft, isRight );
			return resultArray;

		default: throw new Error( );
	}
};

/*
| Intersects this bezier with another figure.
|
| ~figure: figure to intersect with
*/
def.proto.intersectsPoint =
	function( figure )
{
	const at = this.intersectsAt( figure );
	if( at && at.length > 0 )
	{
		return this.pointAt( at[ 0 ] );
	}
	else
	{
		return undefined;
	}
};

/*
| Radiants of angle p0 to pco0.
*/
def.lazy.phi0co0 =
	function( )
{
	const p0 = this.p0;
	const pco0 = this.pco0;

	return atan2( pco0.y - p0.y, pco0.x - p0.x );
};

/*
| Radiants of angle p1 to pco1.
*/
def.lazy.phi1co1 =
	function( )
{
	const p1 = this.p1;
	const pco1 = this.pco1;

	return atan2( pco1.y - p1.y, pco1.x - p1.x );
};

/*
| Radiants of angle pco1 to pco0.
*/
def.lazy.phico1co0 =
	function( )
{
	const pco0 = this.pco0;
	const pco1 = this.pco1;

	return atan2( pco1.y - pco0.y, pco1.x - pco0.x );
};

/*
| Radiants of angle pco1 to p1.
*/
def.lazy.phico11 =
	function( )
{
	const p1 = this.p1;
	const pco1 = this.pco1;

	return atan2( p1.y - pco1.y, p1.x - pco1.x );
};

/*
| Returns point at t (from 0 to 1 ).
*/
def.lazyFunc.pointAt =
	function( t )
{
/**/if( CHECK && !( t >= 0 && t <= 1 ) ) throw new Error( );

	const c = this._coeffs;
	const cx = c.x;
	const cy = c.y;
	const tt = t*t;
	const ttt = tt*t;
	return(
		Point.XY(
			cx[ 0 ]*ttt + cx[ 1 ]*tt + cx[ 2 ]*t + cx[ 3 ],
			cy[ 0 ]*ttt + cy[ 1 ]*tt + cy[ 2 ]*t + cy[ 3 ]
		)
	);
};

/*
| Creation shortcut.
*/
def.static.PointList =
	function( list )
{
	return(
		Self.create(
			'p0', list.get( 0 ),
			'pco0', list.get( 1 ),
			'pco1', list.get( 2 ),
			'p1', list.get( 3 )
		)
	);
};

/*
| Creation shortcut.
*/
def.static.Points =
	function( p0, pco0, pco1, p1 )
{
	return Self.create( 'p0', p0, 'pco0', pco0, 'pco1', pco1, 'p1', p1 );
};

/*
| Returns the 'at' value nearest to 'p'.
*/
def.proto.project =
	function( p )
{
	let nd = Infinity;
	let nt, t;
	for( let it = 0; it <= 100; it++ )
	{
		t = it / 100;
		const pt = this.pointAt( t );
		const d = p.distanceOfPoint( pt );
		if( d < nd ) { nd = d; nt = t; }
	}
	let t0 = nt - 0.01;
	let t1 = nt + 0.01;
	if( t1 > 1 ) t1 = 1;
	if( t0 < 0 ) t0 = 0;
	let d0 = p.distanceOfPoint( this.pointAt( t0 ) );
	let d1 = p.distanceOfPoint( this.pointAt( t1 ) );
	while( t1 - t0 > projectEpsilon )
	{
		nt = ( t0 + t1 ) / 2;
		nd = p.distanceOfPoint( this.pointAt( nt ) );
		if( d0 < d1 ) { t1 = nt; d1 = nd; }
		else { t0 = nt; d0 = nd; }
	}

	return nt;
};

/*
| Reverses the bezier.
|
| (it will envelope in the other direction).
*/
def.lazy.reverse =
	function( )
{
	const r = Self.Points( this.p1, this.pco1, this.pco0, this.p0 );
	ti2c.aheadValue( r, 'reverse', this );
	return r;
};

/*
| If this is a loop, returns the self intersection Point.
| Otherwise false.
*/
def.lazy.selfIntersectsPoint =
	function( )
{
	if( this.type !== 'loop' )
	{
		return false;
	}

	return this.pointAt( this._roots[ 0 ] );
};

/*
| If this is a loop, returns the self intersection at.
| Otherwise false.
*/
def.lazy.selfIntersectsAt =
	function( resultArray )
{
	if( this.type !== 'loop' ) return resultArray;

	if( !resultArray ) resultArray = [ ];

	resultArray.push( this._roots[ 0 ], this._roots[ 1 ] );
	return resultArray;
};

/*
| Moves the bezier by p or x/y.
|
| ~arguments: p or x,y
*/
def.proto.sub =
	function( )
{
	const p0 = this.p0;
	const pco0 = this.pco0;
	const pco1 = this.pco1;
	const p1 = this.p1;

	return(
		this.create(
			'p0', p0.sub.apply( p0, arguments ),
			'pco0', pco0.sub.apply( pco0, arguments ),
			'pco1', pco1.sub.apply( pco1, arguments ),
			'p1', p1.sub.apply( p1, arguments )
		)
	);
};

/*
| Splits the bezier at 't'.
|
| ~t: split at this
| ~array: insert the parts in this array
| ~index: operate at this offset.
| ~remove: remove this number in splice
*/
def.proto.split =
	function( t, array, index, remove )
{
/**/if( CHECK )
/**/{
/**/	if( typeof( t ) !== 'number' ) throw new Error( );
/**/	if( !( t > 0 && t < 1 ) ) throw new Error( );
/**/}

	const p0 = this.p0;
	const pc0 = this.pco0;
	const pc1 = this.pco1;
	const p1 = this.p1;

	const pc0x = pc0.x;
	const pc0y = pc0.y;
	const pc1x = pc1.x;
	const pc1y = pc1.y;
	const m = 1 - t;

	const qc0x = m*(p0.x) + t*pc0x;
	const qc0y = m*(p0.y) + t*pc0y;
	const h0x = m*pc0x + t*pc1x;
	const h0y = m*pc0y + t*pc1y;
	const rc1x = m*pc1x + t*(p1.x);
	const rc1y = m*pc1y + t*(p1.y);
	const qc1x = m*qc0x + t*h0x;
	const qc1y = m*qc0y + t*h0y;
	const rc0x = m*h0x + t*rc1x;
	const rc0y = m*h0y + t*rc1y;
	const qrx = m*qc1x + t*rc0x;
	const qry = m*qc1y + t*rc0y;

	const qc0 = Point.XY( qc0x, qc0y );
	const qc1 = Point.XY( qc1x, qc1y );
	const qr  = Point.XY( qrx, qry );
	const rc0 = Point.XY( rc0x, rc0y );
	const rc1 = Point.XY( rc1x, rc1y );

	const s0 = Self.Points( p0, qc0, qc1, qr  );
	const s1 = Self.Points( qr, rc0, rc1, p1 );

	if( array )
	{
		if( index === undefined ) array.push( s0, s1 );
		else array.splice( index, remove || 0, s0, s1 );
	}
	else array = [ s0, s1 ];
	return array;
};

/*
| Returns a transformed bezier.
*/
def.proto.transform =
	function( transform )
{
	return(
		this.create(
			'p0', this.p0.transform( transform ),
			'pco0', this.pco.transform( transform ),
			'pco1', this.pco.transform( transform ),
			'p1', this.p1.transform( transform )
		)
	);
};

/*
| Returns the type of the bezier.
| 'arch', 'cusp', 'line', 'quadratic' or 'serpentine'
*/
def.lazy.type =
	function( )
{
	// classify aheads type
	this._classify;
	return this.type;
};

/*
| Returns true if point is at most 'distance' far away.
| This may be faster than calling project.
*/
def.proto.withinDistance =
	function( p, distance )
{
	const zd = this.zone.envelope( distance );

	if( !zd.within( p ) ) return false;

	for( let it = 0; it <= 500; it++ )
	{
		const pt = this.pointAt( it / 500 );
		const d = p.distanceOfPoint( pt );
		if( d <= distance ) return true;
	}

	return false;
};

/*
| The zone of the bezier (aka bounding box)
*/
def.lazy.zone =
	function( )
{
	const p0 = this.p0;
	const pco0 = this.pco0;
	const pco1 = this.pco1;
	const p1 = this.p1;

	const p0x = p0.x;
	const p0y = p0.y;
	const pco0x = pco0.x;
	const pco0y = pco0.y;
	const pco1x = pco1.x;
	const pco1y = pco1.y;
	const p1x = p1.x;
	const p1y = p1.y;

	let pmix = min( p0x, p1x );
	let pmiy = min( p0y, p1y );
	let pmax = max( p0x, p1x );
	let pmay = max( p0y, p1y );

	const pcx = pco0x - p0x;
	const pcy = pco0y - p0y;
	const pbx = p0x - 2*pco0x + pco1x;
	const pby = p0y - 2*pco0y + pco1y;

	let pax = 3*( pco0x - pco1x ) + p1x - p0x;
	let pay = 3*( pco0y - pco1y ) + p1y - p0y;

	if( pay === 0 ) pay = 1;
	if( pax === 0 ) pax = 1;

	let phx = pbx*pbx - pax*pcx;
	let phy = pby*pby - pay*pcy;

	if( phx > 0 )
	{
		phx = sqrt( phx );
		let t = ( pbx + phx ) / -pax;
		if( t > 0 && t < 1 )
		{
			const s = 1 - t;
			const qx = s*s*s*p0x + 3*s*s*t*pco0x + 3*s*t*t*pco1x + t*t*t*p1x;
			pmix = min( pmix, qx );
			pmax = max( pmax, qx );
		}
		t = ( phx - pbx ) / pax;
		if( t > 0 && t < 1 )
		{
			const s = 1 - t;
			const qx = s*s*s*p0x + 3*s*s*t*pco0x + 3*s*t*t*pco1x + t*t*t*p1x;
			pmix = min( pmix, qx );
			pmax = max( pmax, qx );
		}
	}

	if( phy > 0 )
	{
		phy = sqrt( phy );
		let t = ( pby + phy ) / -pay;
		if( t > 0 && t < 1 )
		{
			const s = 1 - t;
			const qy = s*s*s*p0y + 3*s*s*t*pco0y + 3*s*t*t*pco1y + t*t*t*p1y;
			pmiy = min( pmiy, qy );
			pmay = max( pmay, qy );
		}
		t = ( phy - pby ) / pay;
		if( t > 0 && t < 1 )
		{
			const s = 1 - t;
			const qy = s*s*s*p0y + 3*s*s*t*pco0y + 3*s*t*t*pco1y + t*t*t*p1y;
			pmiy = min( pmiy, qy );
			pmay = max( pmay, qy );
		}
	}

	return Rect.PosPse( Point.XY( pmix, pmiy ), Point.XY( pmax, pmay ) );
};

/*
| Apportions the bezier into non-sharp parts.
*/
def.lazy._apportion =
	function( )
{
	if( this.flatness <= envelopeMinFlatness ) return this;
	const list = [ this ];
	{
		// split at inflections
		const inflections = this.inflections;
		let offset = 0;
		for( let i = 0, ilen = inflections.length; i < ilen; i++ )
		{
			const qc = list[ i ];
			const split = inflections[ i ];
			// effective split corrected
			const esplit = ( 1 - offset )*split + offset;
			offset = split;
			qc.split( esplit, list, i, 1 );
		}
	}
	let i = 0;
	while( i < list.length )
	{
		const qc = list[ i ];
		if( qc.flatness <= envelopeMinFlatness ) i++;
		else qc.split( 0.5, list, i, 1 );
	}
	return Path.create( 'parts', Parts.create( 'list:init', list ), 'closed', false );
};


/*
| Makes a primitive envelope approximation without splitting.
|
| ~d:         distance to envole
| ~toSegment: if true turns this to a segment
*/
def.proto._approxEnvelope =
	function( d, toSegment )
{
	const tp0 = this.p0;
	const tpc0 = this.pco0;
	const tpc1 = this.pco1;
	const tp1 = this.p1;

	const phi0 = this.phi0co0;
	const phi1 = this.phi1co1;
	const phi0n = phi0 - pi05;
	const phi1n = phi1 + pi05;

	const cos0 = cos( phi0 );
	const sin0 = sin( phi0 );
	const cos1 = cos( phi1 );
	const sin1 = sin( phi1 );
	const cos0n = cos( phi0n );
	const sin0n = sin( phi0n );
	const cos1n = cos( phi1n );
	const sin1n = sin( phi1n );

	// offset by normals
	const p0x = tp0.x + d*cos0n;
	const p0y = tp0.y + d*sin0n;
	const p1x = tp1.x + d*cos1n;
	const p1y = tp1.y + d*sin1n;
	const p0 = Point.XY( p0x, p0y );
	const p1 = Point.XY( p1x, p1y );

	if( toSegment ) return Segment.P0P1( p0, p1 );

	const tl01 = tp0.distanceOfPoint( tp1 );
	let l01 = p0.distanceOfPoint( p1 );

	// for inside cups
	if( Segment.doesIntersect4Points( tp0, p0, tp1, p1 ) ) l01 *= -1;
	const s = l01 / tl01;

	const tl0c0 = tp0.distanceOfPoint( tpc0 );
	const tl1c1 = tp1.distanceOfPoint( tpc1 );
	const l0c0 = tl0c0 * s;
	const l1c1 = tl1c1 * s;

	const pc0x = p0x + l0c0*cos0;
	const pc0y = p0y + l0c0*sin0;
	const pc1x = p1x + l1c1*cos1;
	const pc1y = p1y + l1c1*sin1;

	const pc0 = Point.XY( pc0x, pc0y );
	const pc1 = Point.XY( pc1x, pc1y );

	return Self.Points( p0, pc0, pc1, p1 );
};

/*
| Helper for _classify.
*/
def.proto._classifyTypeRoots =
	function( type, r0, r1 )
{
	let r0k, r1k;
	if( r0 !== undefined )
	{
		r0k = r0 > 0 && r0 < 1;
		r1k = r1 > 0 && r1 < 1;
		if( !( r0k || r1k ) || type === 'loop' && !( r0k && r1k ) )
		{
			ti2c.aheadValue( this, 'type', 'arch' );
			ti2c.aheadValue( this, '_roots', false );
			return;
		}
	}
	if( r0k || r1k )
	{
		if( r0k && r1k )
		{
			const roots = r0 < r1 ? [ r0, r1 ] : [ r1, r0 ];
			ti2c.aheadValue( this, '_roots', Object.freeze( roots ) );
		}
		else
		{
			const roots = r0k ? [ r0 ] : [ r1 ];
			ti2c.aheadValue( this, '_roots', Object.freeze( roots ) );
		}
	}
	else ti2c.aheadValue( this, '_roots', false );
	ti2c.aheadValue( this, 'type', type );
};

/*
| From paper.js
| TODO
*/
def.lazy._classify =
	function( )
{
	const p0 = this.p0;
	const pco0 = this.pco0;
	const pco1 = this.pco1;
	const p1 = this.p1;

	const p0x = p0.x;
	const p0y = p0.y;
	const pc0x = pco0.x;
	const pc0y = pco0.y;
	const pc1x = pco1.x;
	const pc1y = pco1.y;
	const p1x = p1.x;
	const p1y = p1.y;

	const a1 = p0x*( p1y - pc1y ) + p0y*( pc1x - p1x ) + p1x*pc1y - p1y*pc1x;
	const a2 = pc0x*( p0y - p1y ) + pc0y*( p1x - p0x ) + p0x*p1y - p0y*p1x;
	const a3 = pc1x*( pc0y - p0y ) + pc1y*( p0x - pc0x ) + pc0x*p0y - pc0y*p0x;
	let d3 = 3*a3;
	let d2 = d3 - a2;
	let d1 = d2 - a2 + a1;
	const l = sqrt( d1*d1 + d2*d2 + d3*d3 );
	const isZero = EMath.isZero;
	if( l !== 0 ) { d1 /= l; d2 /= l; d3 /= l; }

	if( isZero( d1 ) )
	{
		if( isZero( d2 ) )
			return this._classifyTypeRoots( isZero( d3 ) ? 'line' : 'quadratic', false );
		else
			return this._classifyTypeRoots( 'serpentine', d3/(3*d2 ) );
	}

	const d = 3*d2*d2 - 4*d1*d3;
	if( isZero( d ) ) return this._classifyTypeRoots( 'cusp', d2/(2*d1)  );
	const f1 = d > 0 ? sqrt( d/3 ) : sqrt( -d );
	const f2 = 2 * d1;
	return(
		this._classifyTypeRoots(
			d > 0 ? 'serpentine' : 'loop',
			(d2 + f1)/f2, (d2 - f1)/f2
		)
	);
};

/*
| Bezier coeffizients.
*/
def.lazy._coeffs =
	function( )
{
	const p0 = this.p0;
	const pco0 = this.pco0;
	const pco1 = this.pco1;
	const p1 = this.p1;

	const p0x   = p0.x;
	const p0y   = p0.y;
	const pco0x = pco0.x;
	const pco0y = pco0.y;
	const pco1x = pco1.x;
	const pco1y = pco1.y;
	const p1x   = p1.x;
	const p1y   = p1.y;

	return Object.freeze ( {
		x : Object.freeze(
		[
			-p0x + 3*pco0x - 3*pco1x + p1x,
			3*p0x - 6*pco0x + 3*pco1x,
			-3*p0x + 3*pco0x,
			p0x,
		] ),
		y : Object.freeze(
		[
			-p0y + 3*pco0y - 3*pco1y + p1y,
			3*p0y - 6*pco0y + 3*pco1y,
			-3*p0y + 3*pco0y,
			p0y,
		] )
	} );
};

/*
| Performs fatline clipping this bezier to another bezier.
|
| ~sbez:        subject bezier
| ~calls:       total amount of calls
| ~depth:       depth of algorithm
| ~tt0:         lower bound of this cut
| ~tt1:         upper bound of this cut
| ~st0:         lower bound of subject cut
| ~st1:         uppper bound of subject cut
| ~flip:        polarity of result
| ~resultArray: appends intersections to this array
| ~isLeft:      true if figure is left of this
| ~isRight:     true if figure is right of this
*/
def.proto._fatLineClipping =
	function( sbez, calls, depth, tt0, tt1, st0, st1, flip, resultArray, isLeft, isRight )
{
	if( ++calls >= 4096 || ++depth >= 40) return calls;

	const bsl = this.baseLine;
	const flda = bsl.signedDistanceOfPoint( this.pco0 );
	const fldb = bsl.signedDistanceOfPoint( this.pco1 );
	const factor = flda * fldb > 0 ? 3 / 4 : 4 / 9;
	const fld0 = factor * min( 0, flda, fldb );
	const fld1 = factor * max( 0, flda, fldb );

	const ps0 = Point.XY( 0, bsl.signedDistanceOfPoint( sbez.p0 ) );
	const ps1 = Point.XY( 1, bsl.signedDistanceOfPoint( sbez.p1 ) );
	const psco0 = Point.XY( 1/3, bsl.signedDistanceOfPoint( sbez.pco0 ) );
	const psco1 = Point.XY( 2/3, bsl.signedDistanceOfPoint( sbez.pco1 ) );

	if( ps0.y < fld0 && ps1.y < fld0 && psco0.y < fld0 && psco1.y < fld0 ) return calls;
	if( ps0.y > fld1 && ps1.y > fld1 && psco0.y > fld1 && psco1.y > fld1 ) return calls;

	const lfld0 = Line.P0Angle( Point.XY( 0, fld0 ), Angle.e );
	const lfld1 = Line.P0Angle( Point.XY( 0, fld1 ), Angle.e );

	// presort to lowest can be skipped, since ps0 is always part of hull
	const cvps = ConvexHull.fromPointsP0h( ps0, psco0, psco1, ps1 );

	const hull = [ ];
	for( let a = 0, alen = cvps.length; a < alen; a++ )
	{
		const pa = cvps[ a ];
		const pa2 = cvps[ ( a + 1 ) % alen ];
		hull.push( Segment.P0P1( pa, pa2 ) );
	}

	let x0, x1;
	if( ps0.y < fld0 || ps0.y > fld1 )
	{
		let lf = ps0.y < fld0 ? lfld0 : lfld1;
		for( let ls of hull )
		{
			const pi = ls.intersectsPoint( lf );
			if( !pi ) continue;
			const px = pi.x;
			if( px < 0 || px > 1 ) continue;
			if( x0 === undefined || px < x0 ) x0 = px;
		}
	}

	if( ps1.y < fld0 || ps1.y > fld1 )
	{
		let lf = ps1.y < fld0 ? lfld0 : lfld1;
		for( let ls of hull )
		{
			const pi = ls.intersectsPoint( lf );
			if( !pi ) continue;
			const px = pi.x;
			if( px < 0 || px > 1 ) continue;
			if( x1 === undefined || px > x1 ) x1 = px;
		}
	}

	if( x0 === undefined ) x0 = 0;
	if( x1 === undefined ) x1 = 1;

	const std = st1 - st0;
	st0 += ( std ) * x0;
	st1 -= ( std ) * ( 1 - x1 );

	if( abs( st0 - st1 ) <= flEpsilon )
	{
		const stm = ( st0 + st1 ) / 2;
		const ttm = ( tt0 + tt1 ) / 2;
		if( ( !isRight || ttm <= 1 - neighbourEpsilon )
		&& ( !isLeft || ttm >= neighbourEpsilon )
		)
		{
			if( flip ) resultArray.push( stm, ttm ); else resultArray.push( ttm, stm );
		}
		return calls;
	}

	if( x0 > 0 && x0 < 1 ) sbez = sbez.chipUpper( x0 );
	if( x1 < 1 && x1 > 0 ) sbez = sbez.chipLower( ( x1 - x0 ) / ( 1 - x0 ) );

	if( x1 - x0 > 0.8 )
	{
		const sbl = [ ];
		sbez.split( 0.5, sbl );
		const stm = ( st0 + st1 ) / 2;
		calls =
			sbl[ 0 ]._fatLineClipping(
				this, calls, depth, st0, stm, tt0, tt1, !flip, resultArray, isRight, isLeft
			);

		calls =
			sbl[ 1 ]._fatLineClipping(
				this, calls, depth, stm, st1, tt0, tt1, !flip, resultArray, isRight, isLeft
			);
	}
	else
	{
		calls = sbez._fatLineClipping(
			this, calls, depth, st0, st1, tt0, tt1, !flip, resultArray, isRight, isLeft
		);
	}

	return calls;
};


/*
| Returns the intersection of the bezier with a line.
|
| Returns the intersection ats.
*/
def.proto._intersectsLineAt =
	function( line )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( line.ti2ctype !== Line ) throw new Error( );
/**/}

	const lp0 = line.p0;
	const lp1 = line.p1;

	const lp0x = lp0.x;
	const lp0y = lp0.y;
	const lp1x = lp1.x;
	const lp1y = lp1.y;

	const a = lp1y - lp0y;
	const b = lp0x - lp1x;
	const c = lp0x * ( lp0y - lp1y ) + lp0y*( lp1x - lp0x );

	const coeffs = this._coeffs;
	const cox = coeffs.x;
	const coy = coeffs.y;

	const r =
		EMath.cubicRoots(
			a*cox[ 0 ] + b*coy[ 0 ],    // t^3
			a*cox[ 1 ] + b*coy[ 1 ],    // t^2
			a*cox[ 2 ] + b*coy[ 2 ],    // t
			a*cox[ 3 ] + b*coy[ 3 ] + c // 1
		);

	return Object.freeze( r );
};

/*
| Returns the intersection of the bezier with a line segment.
|
| ~segment:     the figure to find intersections with
| ~extended:    if defined figures are to be extended beyond their end.
| ~flip:        if defined flips results
| ~resultArray: if defined push to this
| ~isLeft:      true if figure is left of this
| ~isRight:     true if figure is right of this
*/
def.proto._intersectsSegmentAt =
	function( segment, extended, flip, resultArray, isLeft, isRight )
{
/**/if( CHECK )
/**/{
/**/	if( segment.ti2ctype !== Segment ) throw new Error( );
/**/}

	const lp0 = segment.p0;
	const lp1 = segment.p1;

	const lp0x = lp0.x;
	const lp0y = lp0.y;
	const lp1x = lp1.x;
	const lp1y = lp1.y;

	const a = lp1y - lp0y;
	const b = lp0x - lp1x;
	const c = lp0x * ( lp0y - lp1y ) + lp0y*( lp1x - lp0x );

	const coeffs = this._coeffs;
	const cox = coeffs.x;
	const coy = coeffs.y;

	const r =
		EMath.cubicRoots(
			a*cox[ 0 ] + b*coy[ 0 ],    // t^3
			a*cox[ 1 ] + b*coy[ 1 ],    // t^2
			a*cox[ 2 ] + b*coy[ 2 ],    // t
			a*cox[ 3 ] + b*coy[ 3 ] + c // 1
		);

	// verifies the roots are in bounds of the line
	if( !resultArray )
	{
		resultArray = [ ];
	}

	for( let i = 0; i < r.length; i++ )
	{
		const at = r[ i ];
		const p = this.pointAt( at );
		const s =
			lp0x !== lp1x  // not a vertical line
			? ( ( p.x - lp0.x ) / ( lp1.x - lp0.x ) )
			: ( ( p.y - lp0.y ) / ( lp1.y - lp0.y ) );

		if( s < 0 || s > 1 ) continue;
		if( isLeft && s >= 1 - neighbourEpsilon ) continue;
		if( isRight && s <= neighbourEpsilon ) continue;

		if( !flip )
		{
			resultArray.push( at, s );
		}
		else
		{
			resultArray.push( s, at );
		}
	}

	return resultArray;
};

/*
| Returns the roots of the bezier.
| Their interpretation depends on the bezier type.
*/
def.lazy._roots =
	function( )
{
	// classify aheads _roots
	this._classify;
	return this._roots;
};

/*
| Creates an outline of a bezier.
| Currently not used.
*/
/*
const outline =
	function( curve, distance )
{
	const env0 = curve.envelope( -distance );
	const env1 = curve.envelope( distance );
	const dm = 30 * Ellipse.magic;

	if( !env0 || !env1 ) return; // failed

	let cap00, cap01;
	{
		const angle0 = curve.angle0;
		const p0 = env1.p0;
		const p1 = env0.p0;
		const cos0 = angle0.cos;
		const sin0 = angle0.sin;
		const p0e = curve.p0.add( cos0 * 30, -sin0 * 30 );
		cap00 =
			Self.Points(
				p0,
				p0.add( cos0 * dm, -sin0 * dm ),
				p0e.add( sin0 * dm, cos0 * dm ),
				p0e
				);
		cap01 =
			Self.Points(
				p0e,
				p0e.add( -sin0 * dm, -cos0 * dm ),
				p1.add( cos0 * dm, -sin0 * dm ),
				p1
			);
	}

	let cap10, cap11;
	{
		const angle1 = curve.angle1;
		const p0 = env0.p1;
		const p1 = env1.p1;
		const p1e = curve.p1.add( angle1.cos * 30, -angle1.sin * 30 );
		const cos1 = angle1.cos;
		const sin1 = angle1.sin;
		cap10 =
			Self.Points(
				p0,
				p0.add( cos1 * dm, -sin1 * dm ),
				p1e.add( sin1 * dm, cos1 * dm ),
				p1e
			);
		cap11 =
			Self.Points(
				p1e,
				p1e.add( -sin1 * dm, -cos1 * dm ),
				p1.add( cos1 * dm, -sin1 * dm ),
				p1
			);
	}

	/*
	let thick;
	{
		const list = env0.parts ? env0.parts.clone( ) : [ env0 ];
		list.push( cap10, cap11 );
		if( env1.ti2ctype === Path )
		{
			// FIXME make path reverse
			for( let part of env1.parts.reverse( ) ) list.push( part.reverse );
		}
		else
		{
			list.push( env1.reverse );
		}
		list.push( cap00, cap01 );
		thick = Path.create( 'parts', PathParts.Array( list ), 'closed', true );
	}

	return thick;

	const si = thick.selfIntersectsAt( );

	// if there are no self intersections already finished
	if( !si || si.length === 0 ) return thick;

	// builds an index array that sorts the intersections
	const oi = [ ];
	for( let a = 0, alen = si.length; a < alen; a++ ) oi.push( a );
	oi.sort( ( a, b ) => si[ a ] - si[ b ] );

	// filters out segments that are within (too near the bezier)
	const segs = [ ];
	const fd = distance * 0.995;
	const from = new Array( oi.length );
	let ip;
	//console.log( '-----------------------' );
	for( let a = 0, alen = oi.length, it; a <= alen; a++, ip = it )
	{
		const atp = ip !== undefined ? si[ ip ] : 0;
		it = oi[ a ];
		const att = a < alen ? si[ it ] : 1;

		const am1 = ( 2 * atp + att ) / 3;
		const am2 = ( atp + att ) / 2;
		const am3 = ( atp + 2 * att ) / 3;
		const win = curve.withinDistance( thick.pointAt( am1 ), fd )
			&& curve.withinDistance( thick.pointAt( am2 ), fd )
			&& curve.withinDistance( thick.pointAt( am3 ), fd );
		if( win ) continue;
		const c = thick.chip( atp, att );

		const iq =
			a < alen
			? ( oi[ a ] % 2 ? oi[ a ] - 1 : oi[ a ] + 1 )
			: undefined;

		//console.log( 'from ' + ip + ' to ' + it + ', ' + iq );

		if( ip !== undefined ) from[ ip ] = segs.length;
		segs.push( c || false, it, iq );
	}

//	const list = [ ];
//	for( let s = 0; s < segs.length; s+= 3 )
//	{
//		list.push( segs[ s ] );
//	}
//	return [ list, env0, env1 ];

	// now cycles are built of the segments to create
	// the finished path
	const paths = [ ];

	const max = segs.length / 3;
	let m = 0;
	while( m < max )
	{
		// skips already processed segments
		let sa = 0;
		while( segs[ sa ] === undefined ) sa += 3;
		const cycle = [ ];
		const visits = new Array( max );

		let isacycle = true;
		for( let steps = 0; m < max; steps++ )
		{
			cycle.push( segs[ sa ] );
			segs[ sa ] = undefined;
			visits[ sa / 3 ] = steps;
			m++;
			if( segs[ sa + 2 ] === undefined ) break;
			const to0 = segs[ sa + 1 ];
			const to1 = segs[ sa + 2 ];
			const f0 = from[ to0 ];
			const f1 = from[ to1 ];
			if( f0 === undefined && f1 === undefined )
			{
				console.log( 'not a circle' );
				isacycle = false;
				break;
			}
			sa = f0 !== undefined ? f0 : f1;

			// checks if has been visited before
			let v = visits[ sa / 3 ];
			if( v !== undefined )
			{
				// undoes the non cycle part
				while( v > 0 ) { cycle.shift( ); v--; }
				break;
			}
		}

		if( isacycle && cycle.length > 0 )
		{
			const flat = [ ];
			for( let c of cycle )
			{
				if( !c ) continue;
				if( c.ti2ctype === Path )
				{
					for( let cs of c.parts ) flat.push( cs );
				}
				else flat.push( c );
			}
			let path;
			if( flat.length > 0 ) path = Path.EpsilonTolerant( flat, true, Infinity );
			if( path ) paths.push( path );
		}
	}

	const outline =
		paths.length === 1
		? paths[ 0 ]
		: FigureList.Array( paths );

	return outline;
};
*/
