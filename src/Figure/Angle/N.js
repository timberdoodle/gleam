/*
| North, 90°
*/
def.extend = 'Figure/Angle/Base';
def.singleton = true;

def.json = true;

import { Self as Angle } from '{Figure/Angle/Self}';

/*
| One intermediate cardinal step counter clockwise.
*/
def.lazy.ccw08 = ( ) => Angle.nw;

/*
| Cosinus value of this angle.
*/
def.proto.cos = 0;

/*
| One intermediate cardinal step clockwise.
*/
def.lazy.cw08 = ( ) => Angle.ne;

/*
| Gets point from a rect.
*/
def.proto.from = ( rect ) => rect.pn;

/*
| Has no x component.
*/
def.proto.hasX = false;

/*
| Has y component.
*/
def.proto.hasY = true;

/*
| Has n component.
*/
def.proto.hasN = true;

/*
| Opposite direction.
*/
def.lazy.opposite = ( ) => Angle.s;

/*
| Radians constant.
*/
def.proto.radians = Math.PI / 2;

/*
| Sinus value of this angle.
*/
def.proto.sin = 1;

/*
| True if the angle is nearer to the y-axis than the x-axis.
*/
def.proto.steep = true;
