/*
| A rectangle with rounded corners.
|
| The ext|ended verion has optional corners
|
|      <--> a
|      |  |
|  pos + .----------------. - - - A - - - A
|      .'                  `. _ _ V b     | height
|      |                    |             |
|      |                    |             |
|      |                    |             |
|      |                    |             |
|      '.                  .'             |
|        `----------------' + - - - - - - V
|      |                    |
|      <--------------------> width
*/
def.attributes =
{
	// position
	pos: { type: 'Figure/Point', json: true },

	// height
	height: { type: 'number', json: true },

	// width
	width: { type: 'number', json: true },

	// horizonal rounding for each corner
	hne: { type: 'number', json: true },
	hnw: { type: 'number', json: true },
	hse: { type: 'number', json: true },
	hsw: { type: 'number', json: true },

	// vertical rounding for each corner
	vne: { type: 'number', json: true },
	vnw: { type: 'number', json: true },
	vse: { type: 'number', json: true },
	vsw: { type: 'number', json: true },
};

import { Self as Display       } from '{Display/Canvas/Self}';
import { Self as Path          } from '{Figure/Path/Self}';
import { Self as TransformBase } from '{Transform/Base}';

def.json = true;

/*
| Shortcut; a reverse c.
*/
def.static.PosWidthHeightHVNese =
	( pos, width, height, h, v ) =>
	Self.create(
		'pos',    pos,
		'height', height,
		'width',  width,
		'hne',    h,
		'hnw',    0,
		'hse',    h,
		'hsw',    0,
		'vne',    v,
		'vnw',    0,
		'vse',    v,
		'vsw',    0,
	);

/*
| Shortcut; top rounded.
*/
def.static.PosWidthHeightHVNeNw =
	( pos, width, height, h, v ) =>
	Self.create(
		'pos',    pos,
		'height', height,
		'width',  width,
		'hne',    h,
		'hnw',    h,
		'hse',    0,
		'hsw',    0,
		'vne',    v,
		'vnw',    v,
		'vse',    0,
		'vsw',    0,
	);

/*
| Shortcut; a forward c.
*/
def.static.PosWidthHeightHVNwsw =
	( pos, width, height, h, v ) =>
	Self.create(
		'pos',    pos,
		'height', height,
		'width',  width,
		'hne',    0,
		'hnw',    h,
		'hse',    0,
		'hsw',    h,
		'vne',    0,
		'vnw',    v,
		'vse',    0,
		'vsw',    v,
	);

/*
| Shortcut; se rounded.
*/
def.static.PosWidthHeightHVSe =
	( pos, width, height, h, v ) =>
	Self.create(
		'pos',    pos,
		'height', height,
		'width',  width,
		'hne',    0,
		'hnw',    0,
		'hse',    h,
		'hsw',    0,
		'vne',    0,
		'vnw',    0,
		'vse',    v,
		'vsw',    0,
	);

/*
| Returns the shape moved by point or x/y.
*/
def.proto.add =
	function( px, y )
{
	return this.create( 'pos', this.pos.add( px, y ) );
};

/*
| Expands( or shrinks ) the round rect by d.
*/
def.lazyFunc.envelope =
	function( d )
{
/**/if( CHECK && typeof( d ) !== 'number' ) throw new Error( );

	const hne = this.hne;
	const hnw = this.hnw;
	const hse = this.hse;
	const hsw = this.hsw;
	const vne = this.vne;
	const vnw = this.vnw;
	const vse = this.vse;
	const vsw = this.vsw;

	return(
		this.create(
			'pos',    this.pos.add( -d, -d ),
			'height', this.height + 2 * d,
			'width',  this.width  + 2 * d,
			'hne',    hne !== 0 ? hne + d : hne,
			'hnw',    hnw !== 0 ? hnw + d : hnw,
			'hse',    hse !== 0 ? hse + d : hse,
			'hsw',    hsw !== 0 ? hsw + d : hsw,
			'vne',    vne !== 0 ? vne + d : vne,
			'vnw',    vnw !== 0 ? vnw + d : vnw,
			'vse',    vse !== 0 ? vse + d : vse,
			'vsw',    vsw !== 0 ? vsw + d : vsw,
		)
	);
};

/*
| Gets the intersections with something else.
|
| ~figure: figure to intersect with
*/
def.proto.intersectsPoint =
	function( figure )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	return this.path.intersectsPoint( figure );
};

/*
| The path of the shape.
*/
def.lazy.path =
	function( )
{
	const h = this.height;
	const p = this.pos;
	const w = this.width;

	const plan = [ 'pc', this.pc ];

	const hne = this.hne;
	const hnw = this.hnw;
	const hse = this.hse;
	const hsw = this.hsw;
	const vne = this.vne;
	const vnw = this.vnw;
	const vse = this.vse;
	const vsw = this.vsw;

	if( vnw !== 0 )
	{
		plan.push( 'start', p.add( 0,   vnw ) );
		plan.push( 'qbend', p.add( hnw, 0   ) );
	}
	else
	{
		plan.push( 'start', p );
	}

	if( vne !== 0 )
	{
		plan.push( 'line',  p.add( w - hne, 0   ) );
		plan.push( 'qbend', p.add( w,       vne ) );
	}
	else
	{
		plan.push( 'line',  p.add( w, 0 ) );
	}

	if( vse !== 0 )
	{
		plan.push( 'line',  p.add( w,       h - vse ) );
		plan.push( 'qbend', p.add( w - hse, h       ) );
	}
	else
	{
		plan.push( 'line',  p.add( w, h ) );
	}

	if( vsw !== 0 )
	{
		plan.push( 'line',  p.add( hsw, h       ) );
		plan.push( 'qbend', p.add( 0,   h - vsw ) );
	}
	else
	{
		plan.push( 'line', p.add( 0, h ) );
	}

	plan.push( 'line', 'close' );

	return Path.Plan.apply( Path.Plan, plan );
};

/*
| Point in the center.
*/
def.lazy.pc =
	function( )
{
	return this.pos.add( this.width / 2, this.height / 2 );
};

/*
| East point.
*/
def.lazy.pe =
	function( )
{
	return this.pos.add( this.width, this.height / 2 );
};

/*
| North point.
*/
def.lazy.pn =
	function( )
{
	return this.pos.add( this.width / 2, 0 );
};

/*
| North east point.
*/
def.lazy.pne =
	function( )
{
	return this.pos.add( this.width, 0 );
};

/*
| South point.
*/
def.lazy.ps =
	function( )
{
	return this.pos.add( this.width / 2, this.height );
};

/*
| South east point.
*/
def.lazy.pse =
	function( )
{
	return this.pos.add( this.width, this.height );
};

/*
| South west point.
*/
def.lazy.psw =
	function( )
{
	return this.pos.add( 0, this.height );
};

/*
| West point.
*/
def.lazy.pw =
	function( )
{
	return this.pos.add( 0, this.height / 2 );
};

/*
| Returns a transformed shape.
*/
def.proto.transform =
	function( transform )
{
/**/if( CHECK && !TransformBase.isa( transform ) ) throw new Error( );

	return(
		this.create(
			'pos',    this.pos.transform( transform ),
			'width',  transform.d( this.width ),
			'height', transform.d( this.height ),
			'hne',    transform.d( this.hne ),
			'hnw',    transform.d( this.hnw ),
			'hse',    transform.d( this.hse ),
			'hsw',    transform.d( this.hsw ),
			'vne',    transform.d( this.vne ),
			'vnw',    transform.d( this.vnw ),
			'vse',    transform.d( this.vse ),
			'vsw',    transform.d( this.vsw ),
		)
	);
};

/*
| Returns true if p is within the shape.
*/
def.proto.within =
	function( p )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	return Display.helper.within( p, this );
};

/*
| Extra checking.
*/
def.proto._check =
	function( )
{
/**/if( CHECK )
/**/{
/**/	const hne = this.hne;
/**/	const hnw = this.hnw;
/**/	const hse = this.hse;
/**/	const hsw = this.hsw;
/**/	const vne = this.vne;
/**/	const vnw = this.vnw;
/**/	const vse = this.vse;
/**/	const vsw = this.vsw;
/**/
/**/	// for a corner either both are zero (hard corner) or neither
/**/	if( ( hne === 0 ) !== ( vne === 0 ) ) throw new Error( );
/**/	if( ( hnw === 0 ) !== ( vnw === 0 ) ) throw new Error( );
/**/	if( ( hse === 0 ) !== ( vse === 0 ) ) throw new Error( );
/**/	if( ( hsw === 0 ) !== ( vsw === 0 ) ) throw new Error( );
/**/}
};
